//
//  WEDownloadPricelistViewController.swift
//  Walna Electrics
//
//  Created by Admin on 04/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEDownloadPricelistViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var priceListTableView: UITableView!
    var downloadListViewModel = DownloadPriceViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let api:String = Constants().qaUrl2 + Constants().downloadBrandsListEndPoint
        downloadListViewModel.fetchDownloadBrandsAndCatalougesList(urlString: api) {[weak self] (resultArray, isSuccess, error) in
            self?.priceListTableView.delegate = self
            self?.priceListTableView.dataSource = self
            self?.priceListTableView.reloadData()
        }

    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WEDownloadPricelistViewController {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return downloadListViewModel.downloadBrandsListCount()!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "downloadPriceCell") as? WEBrandBySubbrandsTableViewCell else {
            fatalError("Cell Doenot exist")
            return UITableViewCell()
        }
        cell.subBrandName.text = downloadListViewModel.getBrand_name(indexPathCell: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WEChoosePDFListViewController") as! WEChoosePDFListViewController
        viewController.downloadPrice_BrandOrCatalouge_EndPoint = Constants().getDownloadableProductPriceListByBrandNameEndPoint
        viewController.downloadPrice_BrandOrCatalouge_Key = "brandId"
        viewController.downloadPrice_BrandOrCatalouge_Value = downloadListViewModel.getBrandId(indexPathCell: indexPath)!
        viewController.modalPresentationStyle = .overCurrentContext
        self.present(viewController, animated: true)
    }
}
