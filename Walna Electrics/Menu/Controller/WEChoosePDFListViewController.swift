//
//  WEChoosePDFListViewController.swift
//  Walna Electrics
//
//  Created by Admin on 04/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
class WEChoosePDFListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,URLSessionDownloadDelegate,UIDocumentInteractionControllerDelegate {
    @IBOutlet weak var catalougeListTableView: UITableView!
    @IBOutlet weak var catalougeListView: UIView!
    @IBOutlet weak var downloadProgressView: UIView!
    @IBOutlet weak var progressBarView: UIProgressView!
    @IBOutlet weak var progressBarValue: UILabel!
    @IBOutlet weak var progressBarPercentageValue: UILabel!

    var brandOrCatalougeListViewModel = DownloadPriceViewModel()
    var downloadPrice_BrandOrCatalouge_EndPoint = ""
    var downloadPrice_BrandOrCatalouge_Key = ""
    var downloadPrice_BrandOrCatalouge_Value = ""
    var progress1 = ""
    var urlLink: URL!
    var defaultSession: URLSession!
    var downloadTask: URLSessionDownloadTask!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        progressBarView.progressViewStyle = .bar
      /*  let api:String = Constants().qaUrl2 + downloadPrice_BrandOrCatalouge_EndPoint
        let params = [downloadPrice_BrandOrCatalouge_Key:downloadPrice_BrandOrCatalouge_Value]
        print("params :- \(params)")
        brandOrCatalougeListViewModel.fetchDownloadProducts_BrandsAndProductsList(urlString: api, method: StringConstant.METHOD_POST, priceBrandOrCatalouge: 1, parameters: params) { (dict, isSuccess, error) in
   
            self.catalougeListTableView.delegate = self
            self.catalougeListTableView.dataSource = self
            self.catalougeListTableView.reloadData()
            

        }
        */

        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
        defaultSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        progressBarView.setProgress(0.0, animated: false)
        
        downloadProgressView.isHidden = false
        
        let imageUrl = "http://appmomos.com/walna//product_price_list/02-Schneider Industrial Automation Products price list-01.07.2017.pdf"
        
        let filterPercentageimageUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
     //   startDownloading(audioUrl:filterPercentageimageUrl)
    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension WEChoosePDFListViewController {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return brandOrCatalougeListViewModel.downloadBrandsOrCatalougesListCount()!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "downloadCatalougeCell") as? WEBrandBySubbrandsTableViewCell else {
            fatalError("Cell Doenot exist")
            return UITableViewCell()
        }
        cell.layer.cornerRadius = 10
        cell.subBrandName.text = brandOrCatalougeListViewModel.downloadedBrandsOrCatalouges_name(indexPathCell: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        downloadProgressView.isHidden = false
        progressBarView.setProgress(0.0, animated: true)
 
        let imageUrl = "http://appmomos.com/walna//product_price_list/02-Schneider Industrial Automation Products price list-01.07.2017.pdf"
        
        let filterPercentageimageUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        startDownloading(audioUrl:"http://appmomos.com/walna//product_price_list/02-Schneider%20Capacitors%20&%20relays%20price%20list%20-01.07.2017.pdf")

    }
    func startDownloading (audioUrl:String) {
        
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
        defaultSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        progressBarView.setProgress(0.0, animated: false)
        downloadTask = defaultSession.downloadTask(with: URL(string: audioUrl)!)
        downloadTask.resume()

    }
    
    func showFileWithPath(path: String){
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true{
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
        
    }
    
    
    // MARK:- URLSessionDownloadDelegate
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        print(downloadTask)
        print("File download succesfully")
        //the stuff from your web request or other method of getting pdf
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = "\(documentsPath)/myCoolPDF.pdf"
        let array = NSArray(contentsOfFile: filePath)
        if let array = array {
            array.write(toFile: filePath, atomically: true)
        }
        
        
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = FileManager()
        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/file12345.pdf"))
        
        if fileManager.fileExists(atPath: destinationURLForFile.path){
            showFileWithPath(path: destinationURLForFile.path)
            print(destinationURLForFile.path)
        }
        else{
            do {
                try fileManager.moveItem(at: location, to: destinationURLForFile)
                // show file
                showFileWithPath(path: destinationURLForFile.path)
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
       // progressBarView.setProgress(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite), animated: true)
        self.progressBarValue.text = String(format: "%.0f", Float(totalBytesWritten)/Float(totalBytesExpectedToWrite) * 100) + "%"
        self.progressBarPercentageValue.text = String(format: "%.0f", Float(totalBytesWritten)/Float(totalBytesExpectedToWrite) * 100) + "/100"
        self.progressBarView.setProgress(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite), animated: true)
    }
   
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        downloadTask = nil
        progressBarView.setProgress(0.0, animated: true)
        if (error != nil) {
            downloadProgressView.isHidden = true
            defaultSession.invalidateAndCancel()
            print("didCompleteWithError \(error?.localizedDescription ?? "no value")")
        }
        else {
            downloadProgressView.isHidden = true
            defaultSession.invalidateAndCancel()

            print("The task finished successfully")
        }
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }

    /*
    startDownload(audioUrl:filterPercentageimageUrl)

    func startDownload(audioUrl:String) -> Void {
       // let fileUrl = self.getSaveFileUrl(fileName: audioUrl)
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (URL(string: audioUrl)!, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(URL(string: audioUrl)!, to:destination)
            .downloadProgress { (progress) in
               // self.progressLabel.text = (String)(progress.fractionCompleted)
                print("////////////////////////   ************ //////////////////////////////")
                print(progress.userInfo)
                self.progressBarValue.text = String(format: "%.0f", progress.fractionCompleted * 100) + "%"
                self.progressBarPercentageValue.text = String(format: "%.0f", progress.fractionCompleted * 100) + "/100"
            self.progressBarView.setProgress(Float(progress.fractionCompleted), animated: true)
            }
            .responseString { (data) in

                print(data.destinationURL)
                print(data.description)
               
                print(data.result.value!)
                //self.createPDF(pdfData: data.result.value!)
                self.progressBarView.setProgress(0.0, animated: true)
            self.downloadProgressView.isHidden = true

            //    self.progressLabel.text = "Completed!"
        }
 
 alamo(audioUrl: audioUrl)
    }
    func showFileWithPath(path: String){
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true{
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            viewer.delegate = self as! UIDocumentInteractionControllerDelegate
            viewer.presentPreview(animated: true)
        }
        
    }
    func createPDF( pdfData : Data) {
        let render = UIPrintPageRenderer()
        
        let html = "<b>Hello <i>World!</i></b> <p>Generate PDF file from HTML in Swift</p>"
        let fmt = UIMarkupTextPrintFormatter(markupText: html)
        
        render.addPrintFormatter(fmt, startingAtPageAt: 0)
        
        // 3. Assign paperRect and printableRect
        
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841.8) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        
        // 4. Create PDF context and draw
        //let pointzero = CGPoint(x: 0,y :0)
        let rect = CGRect.zero
        
        
        let data = NSMutableData(base64Encoded: pdfData.base64EncodedData(), options: NSData.Base64DecodingOptions.ignoreUnknownCharacters);
        
        UIGraphicsBeginPDFContextToData(data!, rect, nil)
        
        
        for i in 1...render.numberOfPages {
            
            UIGraphicsBeginPDFPage();
            let bounds = UIGraphicsGetPDFContextBounds()
            render.drawPage(at: i - 1, in: bounds)
        }
        
        UIGraphicsEndPDFContext();
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        // 5. Save PDF file
        do {
            let fileURL = try documentsPath.asURL().appendingPathComponent("file.pdf")
            try pdfData.write(to: fileURL, options: .atomic)
            
        } catch {
            
        }
        
        
        print("saved success")
       
    }
    func alamo(audioUrl: String) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (URL(string: audioUrl)!, [.removePreviousFile, .createIntermediateDirectories])
        }
        Alamofire.download( audioUrl,
                            method: .get, parameters: nil,
                            encoding: URLEncoding.default, headers: nil, to: destination
            )
            .downloadProgress{ progress in
                print( progress.fractionCompleted )
            }
            .responseString{ response in
                print(response.resumeData as Any)
                if let error = response.error {
                    print( "ERROR: Can't get image for \(audioUrl) \(error)" )
                } else {
                    if let imagePath = response.destinationURL?.path {
                        let image = UIImage( contentsOfFile: imagePath )
                    }
                }
        }
    }
 */
}
