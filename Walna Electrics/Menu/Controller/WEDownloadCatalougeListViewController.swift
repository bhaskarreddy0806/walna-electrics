//
//  WEDownloadCatalougrListViewController.swift
//  Walna Electrics
//
//  Created by Admin on 04/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEDownloadCatalougeListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var catalougeListTableView: UITableView!
    var catalougeListViewModel = DownloadPriceViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let api:String = Constants().qaUrl2 + Constants().downloadCatalougeListEndPoint
        catalougeListViewModel.fetchDownloadBrandsAndCatalougesList(urlString: api) {[weak self] (resultArray, isSuccess, error) in
            self?.catalougeListTableView.delegate = self
            self?.catalougeListTableView.dataSource = self
            self?.catalougeListTableView.reloadData()
        }
        
    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension WEDownloadCatalougeListViewController {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return catalougeListViewModel.downloadBrandsListCount()!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "downloadCatalougeCell") as? WEBrandBySubbrandsTableViewCell else {
            fatalError("Cell Doenot exist")
            return UITableViewCell()
        }
        cell.subBrandName.text = catalougeListViewModel.getBrand_name(indexPathCell: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WEChoosePDFListViewController") as! WEChoosePDFListViewController
        viewController.downloadPrice_BrandOrCatalouge_EndPoint = Constants().getDownloadableProductPriceCatalougeListEndPoint
        viewController.downloadPrice_BrandOrCatalouge_Key = "catalogueId"
        viewController.downloadPrice_BrandOrCatalouge_Value = catalougeListViewModel.getBrandId(indexPathCell: indexPath)!
        viewController.modalPresentationStyle = .overCurrentContext
        self.present(viewController, animated: true)
    }
}
