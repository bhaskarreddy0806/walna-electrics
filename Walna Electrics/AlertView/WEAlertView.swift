//
//  WEAlertView.swift
//  Walna Electrics
//
//  Created by Bhaskar on 19/04/18.
//  Copyright © 2018 Bhaskar. All rights reserved.
//

import UIKit
protocol WEAlertViewDelegate {
    func okButtonTapped()
    func cancelButtonTapped()
}
class WEAlertView: UIView {
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var alertMessageLabel: UILabel!

    var delegate:WEAlertViewDelegate?
    
   class func alertViewCustom() -> WEAlertView {
    
    let myClassNib = UINib(nibName: "WEAlertView", bundle: nil)

    return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! WEAlertView
    }
    func setMessage(message: String){
        alertMessageLabel.text = message
        okButton.setTitle("Ok", for: UIControlState.normal)
        cancelButton.setTitle("Cancel", for: UIControlState.normal)
    }
    @IBAction func okButtonAction(_ sender: Any) {
        delegate?.okButtonTapped()
    }
    @IBAction func cancelButtonAction(_ sender: Any) {
        delegate?.cancelButtonTapped()
        self.removeFromSuperview()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
