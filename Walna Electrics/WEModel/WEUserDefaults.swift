//
//  WEUserDefaults.swift
//  Walna Electrics
//
//  Created by Admin on 27/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit

var cartCountDynamic: String? {
    get {
        return UserDefaults.standard.string(forKey: "cart_key") ?? "0"
    }
    set (newValue) {
        UserDefaults.standard.set(newValue, forKey:"cart_key")
        UserDefaults.standard.synchronize()
    }
}
var userId: String? {
    get {
        return UserDefaults.standard.string(forKey: "userId_key") ?? "3"
    }
    set (newValue) {
        UserDefaults.standard.set(newValue, forKey:"userId_key")
        UserDefaults.standard.synchronize()
    }
}
var isGuestUser: Bool? {
    get {
        return UserDefaults.standard.bool(forKey: "key_isGuestUser")
    }
    set (newValue) {
        UserDefaults.standard.bool(forKey: "key_isGuestUser")
        UserDefaults.standard.synchronize()
    }
}
var userName: String? {
    get {
        return UserDefaults.standard.string(forKey: "key_username") ?? "0"
    }
    set (newValue) {
        UserDefaults.standard.set(newValue, forKey:"key_username")
        UserDefaults.standard.synchronize()
    }
}
class NDUtility: NSObject
{
    class func pushToStoryBoard(target:UIViewController,controllerIdentifier:String,storyBoard:String){
        let storyboard = UIStoryboard(name: storyBoard, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: controllerIdentifier)
        target.navigationController?.pushViewController(viewController, animated: false)
    }
    class func navigationBarHeightConstrants()->CGFloat {
        return 55.0
    }
    class func tabBarHeightConstrants() -> CGFloat {
        return 55.0
    }
    class func tableViewHeight() -> CGFloat {
        return 120.0
    }
    class func menuCellHeight() -> CGFloat {
        return 45.0
}
}
