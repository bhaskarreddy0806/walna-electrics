//
//  WEConstants.swift
//  walna_latest Electrics
//
//  Created by Admin on 19/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
let loadImage = "load.png"

struct Constants {

     let urlPostfix = "&key=aQwerty"
     let qaUrl2 = "http://appmomos.com/"
    //Brands
    
    let bannersEndPoint = "walna_latest/index.php?route=feed/rest_api_home/getBanner&key=aQwertya"
    let shopByManufacturesEndPoint = "walna_latest/index.php?route=feed/rest_api_home/shopByValuedManufacturers&key=aQwertya"
    let brandsAndSubBrandsEndPoint = "walna_latest/index.php?route=feed/new_change_api/getBrandsAndSubBrands"

//all product for search only in home page
    // update customer by token for guest user
    
    ///
    let  getSubBrandsByBrandEndPoint = "walna_latest/index.php?route=feed/new_change_api/getSubBrandsByBrand&key=aQwerty"
    let  getSubSubBrandsBySubBrandNameEndPoint = "walna_latest/index.php?route=feed/new_change_api/getSubSubBrandsBySubBrandName&key=aQwerty"
    let getProductsBySubSubBrandsEndPoint = "walna_latest/index.php?route=feed/new_change_api/getProductsBySubSubBrandName&key=aQwerty"
    let getAllBrandNamesEndPoint = "walna_latest/index.php?route=feed/new_change_api/getBrandNames&key=aQwerty"
    // products Module
    let getAllProductNamesEndPoint = "walna_latest/index.php?route=feed/new_change_api/getProductsName&key=aQwerty"
    let getCategoriesByProductsNameEndPoint = "walna_latest/index.php?route=feed/new_change_api/getCategoriesByProductsName&key=aQwerty"
    let getProductsByCategoryNameEndPoint = "walna_latest/index.php?route=feed/new_change_api/getProductsByCategoryName&key=aQwerty"
    
    // Cart List
    let addToCartEndPoint = "walna_latest/index.php?route=feed/coupon_api/addToCart&key=aQwertya"
    let getCartProductsEndPoint = "walna_latest/index.php?route=feed/coupon_api/getCartProducts&key=aQwertya"
    let removeCartProductsEndPoint = "walna_latest/index.php?route=feed/rest_api_home/remove&key=aQwertya"
    let clearCartEndPoint = "walna_latest/index.php?route=feed/coupon_api/clearcart&key=aQwerty"
    
    // Wish List(Favorite)
    let addWishlistEndPoint = "walna_latest/index.php?route=feed/rest_api/addWishlist&key=aQwertya"
    let getWishlistEndPoint = "walna_latest/index.php?route=feed/rest_api/getWishlist&key=aQwertya"
    let deleteWishlistEndPoint = "walna_latest/index.php?route=feed/rest_api/deleteWishlist&key=aQwertya"
    let clearWishlistEndPoint = "walna_latest/index.php?route=feed/coupon_api/clearWishlist&key=aQwerty"

   //Not required let getTo talWishlistEndPoint = "walna_latest/index.php?route=feed/rest_api/getTotalWishlist&key=aQwertya"
    let notifyCustomerWishListEndPoint = "walna_latest/index.php?route=feed/coupon_api/notifyCustomer&key=aQwertya"
    
    // Download price list
    let downloadBrandsListEndPoint = "walna_latest/index.php?route=feed/new_change_api/getDownloadableBrands&key=aQwerty"
    let getDownloadableProductPriceListByBrandNameEndPoint = "walna_latest/index.php?route=feed/new_change_api/getDownloadableProductPriceListByBrandName&key=aQwerty"

    //Download catalouge list
    let downloadCatalougeListEndPoint = "walna_latest/index.php?route=feed/new_change_api/getDownloadableCatalogues&key=aQwerty"
    let getDownloadableProductPriceCatalougeListEndPoint = "walna_latest/index.php?route=feed/new_change_api/getDownloadableProductPriceListByCatalogueName&key=aQwerty"
    
    //My Account Api End points
    let editTelephoneEndPoint = "walna_latest/index.php?route=feed/coupon_api/editTelephone&key=aQwerty"
    let addAddressEndPoint = "walna_latest/index.php?route=feed/coupon_api/addAddress&key=aQwerty"
    let getAddressEndPoint = "walna_latest/index.php?route=feed/coupon_api/getAddresses&key=aQwerty"
    let deleteAddressEndPoint = "walna_latest/index.php?route=feed/coupon_api/deleteAddress&key=aQwerty"
    let setDefaultAddressEndPoint = "walna_latest/index.php?route=feed/coupon_api/setDefaultAddress&key=aQwertya"
    // update password/ edit password
    
    // Register
    let registrationEndPoint = "walna_latest/index.php?route=feed/coupon_api/getCustomerSupportDetails&key=aQwertya"
    let loginEndPoint = "walna_latest/index.php?route=feed/rest_api/login&key=aQwertya"
    let getCustomerSupportDetailsEndPoint = "walna_latest/index.php?route=feed/coupon_api/getCustomerSupportDetails&key=aQwertya"
}
