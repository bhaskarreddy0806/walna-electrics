//
//  Model.swift
//  MLM
//
//  Created by Rajath on 14/07/17.
//  Copyright © 2017 TDB. All rights reserved.
//

import SwiftyJSON

internal protocol ModelParser {
    init?(json: JSON)
}
extension JSON {
    func to<T>(type: T?) -> Any? {
        if let baseObj = type as? JSON.Type {
            if self.type == .array {
                var arrObject: [Any] = []
                for obj in self.arrayValue {
                    let object = baseObj.init(obj)
                    arrObject.append(object)
                }
                return arrObject
            } else {
                let object = baseObj.init(self)
                return object
            }
        }
        return nil
    }
}
extension Array {
    subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
    func arrayOfInstanse<T>() -> [T]? where T: ModelParser {
        var items: [T] = []
        for item in self {
            if let item = item as? JSON, let finalItem = T(json: item) {
                items.append(finalItem)
            }
        }
        return items.isEmpty ? nil : items
    }
    
    func mapToString() -> [String]? {
        var items: [String] = []
        for item in self {
            if let item = item as? JSON {
                items.append(item.stringValue)
            }
        }
        return items.isEmpty ? nil : items
    }
}
