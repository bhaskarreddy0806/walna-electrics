//
//  LoadingIndicator.swift
//  MLM
//
//  Created by Rajath on 25/07/17.
//  Copyright © 2017 TDB. All rights reserved.
//

import UIKit

internal enum LoadingStyle {
    //gray opaque background
    case fullScreen
     //clear transparent background
    case center
}

internal class LoadingIndicator {
    
    static var progress: NDActivityController?
    
    static func showActivityIndicator(message: String? = nil,
                                      loadingStyle: LoadingStyle = .fullScreen) {
        
        if LoadingIndicator.progress != nil {
            return
        }
        LoadingIndicator.progress = NDActivityController(nibName: "NDActivityController", bundle: nil)
        
        if loadingStyle == .center {
            LoadingIndicator.progress?.view.backgroundColor = UIColor.clear
        }
        LoadingIndicator.progress?.loadingMessage = message
        if let targetVC = UIApplication.shared.keyWindow?.rootViewController {
            LoadingIndicator.progress?.startActivity(target: targetVC)
        }
    }
    
    static func dismiss() {
        LoadingIndicator.progress?.stopActivity()
        LoadingIndicator.progress = nil
    }
}
