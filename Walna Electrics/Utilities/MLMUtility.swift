//
//  MLMUtility.swift
//  MLM
//
//  Created by Nikhil Ravan on 16/06/17.
//  Copyright © 2017 TDB. All rights reserved.
//

import UIKit

internal class MLMUtility: NSObject {
    
    // MARK: Push Navigation View Controller
    
    internal class func pushToStoryBoard(target: UIViewController, controllerIdentifier: String, storyBoard: String) {
        let storyboard = UIStoryboard(name: storyBoard, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: controllerIdentifier)
        target.navigationController?.pushViewController(viewController, animated: true)
    }
    
    internal class func converTimeStampToString(time: Double) -> String {
        let unixTimestamp = time
        let date = Date(timeIntervalSince1970: unixTimestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let strDate = dateFormatter.string(from: date as Date)
        return strDate
    }
    
    internal class func customGreenColor() -> UIColor {
        return #colorLiteral(red: 0, green: 0.3294117647, blue: 0.2156862745, alpha: 1)
    }
    
    internal class func customFadeGreenColor() -> UIColor {
        return #colorLiteral(red: 0, green: 0.3294117647, blue: 0.2156862745, alpha: 0.5)
    }
    
    internal class func customMediumYellow() -> UIColor {
        return #colorLiteral(red: 0.9490196078, green: 0.7803921569, blue: 0.01568627451, alpha: 1)
    }
    
    internal class func customYellowColor() -> UIColor {
        return #colorLiteral(red: 0.9490196078, green: 0.8235294118, blue: 0.01568627451, alpha: 1)
    }
    
    internal class func addImageOnTextField(_ textField: UITextField, imageName: String, iconWidth: CGFloat, iconHeight: CGFloat) {
        let textFieldView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: textField.frame.size.height))
        let imageView = UIImageView(frame:CGRect(x: ((textFieldView.frame.size.width / 2) - (iconWidth / 2)),
                                                 y: ((textFieldView.frame.size.height / 2) - (iconHeight / 2)),
                                                 width: iconWidth, height: iconHeight))
        imageView.image = UIImage(named : imageName)
        textField.rightView = textFieldView
        textField.rightViewMode = .always
        textField.rightView = textFieldView
        textFieldView.addSubview(imageView)
    }
    
    internal class func addSearchImageOnTextField(textField: UITextField, imageName: String, iconWidth: CGFloat, iconHeight: CGFloat) {
        let textFieldView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: textField.frame.size.height))
        let imageView = UIImageView(frame:CGRect(x: ((textFieldView.frame.size.width / 2) - (iconWidth / 2)),
                                                 y: ((textFieldView.frame.size.height / 2) - (iconHeight / 2)),
                                                 width: iconWidth, height: iconHeight))
        imageView.image = UIImage(named : imageName)
        textFieldView.addSubview(imageView)
        textField.leftView = textFieldView
        textField.leftViewMode = .always
    
    }
}

extension UIImageView {
    func imageFromServerURL(urlString: String) {
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, _, error) -> Void in
            if error != nil {
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let storage = HTTPCookieStorage.shared
                for cookie in storage.cookies! {
                    storage.deleteCookie(cookie)
                }
                var libraryPath: String = FileManager().urls(for: .libraryDirectory, in: .userDomainMask).first!.path
                libraryPath += "/Cookies"
                do {
                    try FileManager.default.removeItem(atPath: libraryPath)
                } catch _ as NSError {
                }
                var libraryPathCaches: String = FileManager().urls(for: .libraryDirectory, in: .userDomainMask).first!.path
                libraryPathCaches += "/Caches"
                do {
                    try FileManager.default.removeItem(atPath: libraryPathCaches)
                } catch _ as NSError {
                }
                URLCache.shared.removeAllCachedResponses()
                URLCache.shared.diskCapacity = 0
                URLCache.shared.memoryCapacity = 0
                let image = UIImage(data: data!)
                self.image = image
            })
        }).resume()
    }
}
