//
//  Config.swift
//  MLM
//
//  Created by Rajath on 29/07/17.
//  Copyright © 2017 TDB. All rights reserved.
//

import Foundation

internal struct Config {

    static let isCredEncripted = true
    static let isAPIEncrypted = true
}
