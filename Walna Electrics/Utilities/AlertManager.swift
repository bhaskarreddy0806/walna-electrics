//
//  AlertManager.swift
//  MLM
//
//  Created by Rajath on 22/07/17.
//  Copyright © 2017 TDB. All rights reserved.
//

import SwiftMessages

internal class CustomAlertView: MessageView {
    
    var skipAction: (() -> Void)?
    
    @IBAction func actionHandler(_ sender: UIButton) {
        SwiftMessages.hide()
        skipAction?()
    }
}

internal class MessageManager {
    
    static func showAlert(title: String,
                          message: String,
                          imageName: String?) {
        showAlert(title: title, message: message, imageName: imageName) {
            // Nothing to do
        }
    }
    
    static func showAlert(title: String,
                          message: String,
                          imageName: String?,
                          skipHandler: @escaping () -> Void) {
        
        let view: CustomAlertView? = try? SwiftMessages.viewFromNib()
        var image: UIImage? = nil
        if let imageName = imageName {
            image = UIImage(named: imageName)
        }
        view?.configureContent(title: title, body: message, iconImage: image, iconText: nil, buttonImage: nil, buttonTitle: "OK", buttonTapHandler: nil)
        view?.configureDropShadow()
        view?.skipAction = skipHandler
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        config.duration = .forever
        config.presentationStyle = .center
        config.dimMode = .gray(interactive: true)
        if let view = view {
            SwiftMessages.show(config: config, view: view)
        }
    }
}
