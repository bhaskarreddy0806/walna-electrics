//
//  WESubbrandsByBrandViewController.swift
//  Walna Electrics
//
//  Created by Admin on 23/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WESubbrandsByBrandViewController: UIViewController {
    @IBOutlet weak var subbrandsByBrandTableView: UITableView!
    @IBOutlet weak var navigationTitle: UILabel!
    
    var viewModel = SubBrandsByBrandViewModel()
    var brandName = ""
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitle.text = brandName
        let api:String = Constants().qaUrl2 + Constants().getSubBrandsByBrandEndPoint
        viewModel.fetchSubbrandsByBrand(urlString: api, method: StringConstant.METHOD_POST, parameters: ["brandName":"\(String(describing: brandName))"]){[weak self] () in
            self?.subbrandsByBrandTableView.reloadData()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension WESubbrandsByBrandViewController: UITableViewDelegate, UITableViewDataSource {
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfItemsToDisplay()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subbrandsByBrand") as! WESubbrandsByBrandTableViewCell
        cell.backgroundColor = .white
        cell.subBrandName.text =  viewModel.subBrandNameToDisplay(cellIndexPath: indexPath)
        cell.brandImage.imageFromServerURL(urlString: viewModel.subBrandImage(cellIndexPath: indexPath))
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected indexpath.row \(indexPath.section) row")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WESubbrandsViewController") as! WESubbrandsViewController
        viewController.subBrandName = viewModel.subBrandNameToDisplay(cellIndexPath: indexPath)
        self.present(viewController, animated: true)
    }
}

