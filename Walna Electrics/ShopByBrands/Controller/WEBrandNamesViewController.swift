//
//  WEBrandNamesViewController.swift
//  Walna Electrics
//
//  Created by Admin on 22/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEBrandNamesViewController: UIViewController {
    @IBOutlet weak var brandNamesTableView: UITableView!
    @IBOutlet weak var navigationTitle: UILabel!
    
    var viewModel = AllBrandsNamesViewModel()
    var brandName = ""
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .green
        // Do any additional setup after loading the view.
        navigationTitle.text = brandName
        let api:String = Constants().qaUrl2 + Constants().getAllBrandNamesEndPoint
        viewModel.fetchAllBrandNamesList(urlString: api){[weak self] () in
            self?.brandNamesTableView.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension WEBrandNamesViewController: UITableViewDelegate, UITableViewDataSource {
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "brandBySubbrands") as! WEBrandBySubbrandsTableViewCell
        cell.backgroundColor = .white
        cell.subBrandName.text =  viewModel.brandNameToDisplay(cellIndexPath: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected indexpath.row \(indexPath.section) row")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WESubbrandsByBrandViewController") as? WESubbrandsByBrandViewController
        viewController?.brandName = viewModel.brandNameToDisplay(cellIndexPath: indexPath)
        self.present(viewController!, animated: true)
    }
}

