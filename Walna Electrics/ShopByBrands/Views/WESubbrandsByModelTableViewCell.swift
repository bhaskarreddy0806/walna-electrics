//
//  WESubbrandsByModelTableViewCell.swift
//  Walna Electrics
//
//  Created by Admin on 23/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WESubbrandsByBrandTableViewCell: UITableViewCell {
    @IBOutlet weak var subBrandName: UILabel!
    @IBOutlet weak var brandImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
