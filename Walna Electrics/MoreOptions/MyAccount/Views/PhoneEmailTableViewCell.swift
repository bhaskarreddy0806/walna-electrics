//
//  Phone_EmailTableViewCell.swift
//  Walna Electrics
//
//  Created by Admin on 07/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class PhoneEmailTableViewCell: UITableViewCell {
    @IBOutlet weak var addShippingButton: UIButton!
    @IBOutlet weak var updatePhonenumberButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
