//
//  WEChangePassword.swift
//  Walna Electrics
//
//  Created by Admin on 07/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEChangePassword: UIView {

    @IBOutlet weak var newpasswordTextfield: UITextField!
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!

    class func createMyClassView() -> WEChangePassword {
        let myClassNib = UINib(nibName: "WEChangePassword", bundle: nil)
        return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! WEChangePassword
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    @IBAction func updatePasswordButtonAction(_ sender: Any) {
        
     }
     /*
     // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
