//
//  WEUpdatePhonenumberView.swift
//  Walna Electrics
//
//  Created by Admin on 07/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
protocol WEUpdatePhonenumberViewDelegate: class {
    func request_updatePhonenumberApi(phoneNumber: String?)
}
class WEUpdatePhonenumberView: UIView {

    @IBOutlet weak var phoneNumberTextfield: UITextField!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    weak var delegate: WEUpdatePhonenumberViewDelegate?
    class func createMyClassView() -> WEUpdatePhonenumberView {

        let myClassNib = UINib(nibName: "WEUpdatePhonenumberView", bundle: nil)
        return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! WEUpdatePhonenumberView
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    @IBAction func updatePhoneNumberButtonAction(_ sender: Any) {
        delegate?.request_updatePhonenumberApi(phoneNumber: phoneNumberTextfield.text)
     }
     /*
     // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
