//
//  LastNameTableViewCell.swift
//  Walna Electrics
//
//  Created by Admin on 08/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class LastNameTableViewCell: UITableViewCell {
    @IBOutlet weak var lastnameTextField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
