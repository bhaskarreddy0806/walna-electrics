//
//  AddAddressTableViewCell.swift
//  Walna Electrics
//
//  Created by Admin on 08/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class AddAddressTableViewCell: UITableViewCell, UITextViewDelegate {
    @IBOutlet weak var addressTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addressTextView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
