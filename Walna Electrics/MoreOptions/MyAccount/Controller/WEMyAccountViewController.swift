//
//  WEMyAccountViewController.swift
//  Walna Electrics
//
//  Created by Admin on 07/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEMyAccountViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var myAccountTableView: UITableView!
    let updatePhonenumberView = WEUpdatePhonenumberView.createMyClassView()
    var activityView = NDActivityViewHelper()

    var addressViewModel = AddressViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCell()
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        getAddressApiRequest()
    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    func registerTableViewCell() {
        // Do any additional setup after loading the view.
        let nib = UINib(nibName: "PhoneEmailTableViewCell", bundle: nil)
        myAccountTableView.register(nib, forCellReuseIdentifier: "phoneEmail")
    }
    func getAddressApiRequest() {
        let params: Dictionary = ["customer_id":userId as Any]
        let api:String = Constants().qaUrl2 + Constants().getAddressEndPoint
        
        addressViewModel.getMultipleAddressFromMyaccount(urlString: api, method: StringConstant.METHOD_POST, parameters: params ){[weak self] (message, isSuccess) in
            if (isSuccess) {
            self?.myAccountTableView.reloadData()
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
        return 177
        }
        if indexPath.section == 1 {
            return 152
        }
        if indexPath.section == 2 {
            if let text = addressViewModel.address_address(indexPathCell: indexPath) {

                return (heightForText(text: text, customSize: UIFont.systemFont(ofSize: 18.0), Width: 250) ) + 160
        }
        return 160
        }
        if indexPath.section == 3 {
            return 76
        }
        return UITableViewAutomaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 2 {
            return addressViewModel.addressesListCount()!
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "picWithUsername", for: indexPath)
            return cell
        }
        
        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addressUpdate", for: indexPath) as? UpdateAddrresTableViewCell
            if addressViewModel.isDefaultAddress(indexPathCell: indexPath) == true {
                cell?.bgView?.backgroundColor = .orange
            } else {
                    cell?.bgView?.backgroundColor = .white
            }
            cell?.firstLastNameLabel.text = addressViewModel.address_firstAndLastName(indexPathCell: indexPath)
            cell?.address.text = addressViewModel.address_address(indexPathCell: indexPath)
            cell?.cityLabel.text = addressViewModel.address_city(indexPathCell: indexPath)
            cell?.pinCodeLabel.text = addressViewModel.address_zipcode(indexPathCell: indexPath)
            cell?.clickToMarkButton?.addTarget(self
                , action: #selector(clickToMarkButtonAction(_:)), for: .touchUpInside)
            cell?.deleteAddrresButton?.addTarget(self
                , action: #selector(deleteAddrresButtonAction(_:)), for: .touchUpInside)
            return cell!
        }
        if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "changePWD", for: indexPath) as? ChangePasswordTableViewCell
            cell?.changePasswordButton.addTarget(self
                , action: #selector(changePWDButtonAction(_:)), for: .touchUpInside)
            return cell!
        }
        if indexPath.section == 1 {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "phoneEmail", for: indexPath) as?  PhoneEmailTableViewCell else {
            return UITableViewCell()
        }
            cell.updatePhonenumberButton?.addTarget(self
                , action: #selector(updatePhonenumberButtonAction(_:)), for: .touchUpInside)
            cell.addShippingButton?.addTarget(self
                , action: #selector(addShippingButtonAction(_:)), for: .touchUpInside)
            return cell
        }
        // Configure the cell...
        return UITableViewCell()
    }
    func heightForText(text: String,customSize: UIFont,Width: CGFloat) -> CGFloat{
        
        let constrainedSize = CGSize.init(width:Width, height: CGFloat(MAXFLOAT))
        
        let attributesDictionary = NSDictionary.init(object: customSize, forKey:NSAttributedStringKey.font as NSCopying)
        
        let mutablestring = NSAttributedString.init(string: text, attributes: attributesDictionary as? [NSAttributedStringKey : Any])
        
        var requiredHeight = mutablestring.boundingRect(with:constrainedSize, options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), context: nil)
        
        if requiredHeight.size.width > Width {
            requiredHeight = CGRect.init(x: 0, y: 0, width: Width, height: requiredHeight.height)
            
        }
        return requiredHeight.size.height;
    }
    @objc func clickToMarkButtonAction(_ sender: AnyObject) {
        activityView.startActivity(target: self)
        let buttonPosition = sender.convert(CGPoint.zero, to: self.myAccountTableView)
        let indexPath = self.myAccountTableView.indexPathForRow(at:buttonPosition)
        let params = ["customer_id":userId,"address_id":addressViewModel.address_Id(indexPathCell: indexPath!)]
        let api:String = Constants().qaUrl2 + Constants().setDefaultAddressEndPoint
        addressViewModel.setDefaultAddressToMyaccount(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (message, isSuccess) in
            self?.updatePhonenumberView.removeFromSuperview()
            self?.activityView.stopActivity()
            MessageManager.showAlert(title: "Walna Electrics", message: message!, imageName: nil)
            self?.getAddressApiRequest()
            
        }
    }
    @objc func deleteAddrresButtonAction(_ sender: AnyObject) {
        activityView.startActivity(target: self)
        let buttonPosition = sender.convert(CGPoint.zero, to: self.myAccountTableView)
        let indexPath = self.myAccountTableView.indexPathForRow(at:buttonPosition)
        let params = ["customer_id":userId,"address_id":addressViewModel.address_Id(indexPathCell: indexPath!)]
        let api:String = Constants().qaUrl2 + Constants().deleteAddressEndPoint
        addressViewModel.deleteAddressFromMyaccount(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (message, isSuccess) in
            self?.updatePhonenumberView.removeFromSuperview()
            self?.activityView.stopActivity()
            MessageManager.showAlert(title: "Walna Electrics", message: message!, imageName: nil)
            self?.getAddressApiRequest()

        }
    }
    @objc func addShippingButtonAction(_ sender: AnyObject) {
    let storyboard = UIStoryboard(name: "MoreOptions", bundle: nil)
    let viewController = storyboard.instantiateViewController(withIdentifier :"WEAddAddressViewController") as! WEAddAddressViewController
    viewController.modalPresentationStyle = .overCurrentContext
    self.present(viewController, animated: true)
 
    }
    @objc func updatePhonenumberButtonAction(_ sender: AnyObject) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.myAccountTableView)
        let indexPath = self.myAccountTableView.indexPathForRow(at:buttonPosition)
        print("indexPath selected :- \(indexPath?.section ?? 566 )")
        updatePhonenumberView.frame = self.view.frame
        updatePhonenumberView.delegate = self
        self.view.addSubview(updatePhonenumberView)
        
    }
    @objc func changePWDButtonAction(_ sender: AnyObject) {
        let buttonPosition = sender.convert(CGPoint.zero, to: myAccountTableView)
        let indexPath = self.myAccountTableView.indexPathForRow(at:buttonPosition)
        var changePwdView = WEChangePassword.createMyClassView()
        changePwdView.frame = self.view.frame
        self.view.addSubview(changePwdView)
    }

}
//MARK: -Delegates to Api call
extension WEMyAccountViewController: WEUpdatePhonenumberViewDelegate {
    func request_updatePhonenumberApi(phoneNumber:String?) {
        activityView.startActivity(target: self)

       let params = ["customer_id":userId]
        let api:String = Constants().qaUrl2 + Constants().getAddressEndPoint
        
        addressViewModel.updatePhoneNumberToMyaccount(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (message, isSuccess) in
            self?.updatePhonenumberView.removeFromSuperview()
            self?.activityView.stopActivity()
            MessageManager.showAlert(title: "Walna Electrics", message: message!, imageName: nil)

        }
        
    }
    
}
