//
//  WEAddAddressViewController.swift
//  Walna Electrics
//
//  Created by Admin on 08/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEAddAddressViewController: UIViewController
    , UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var myAddressTableView: UITableView!
    var addressViewModel = AddressViewModel()
    var activityView = NDActivityViewHelper()

    var dynamicHeight: CGFloat = 44
    var firstName: String?
    var lastName: String?
    var address: String?
    var city: String?
    var pincode: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        myAddressTableView.estimatedRowHeight = 44
        myAddressTableView.rowHeight = UITableViewAutomaticDimension
       // myAddressTableView.register(AddAddressTableViewCell.self, forCellReuseIdentifier: "add_address")
        myAddressTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 50
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.size.width, height:50)
        headerView.backgroundColor = .white
        
        let titleLabel = UILabel()
        titleLabel.frame = CGRect(x:5, y:10, width:UIScreen.main.bounds.size.width - 10, height:34)
        titleLabel.backgroundColor = .clear
        titleLabel.textColor = .darkGray
        titleLabel.text = "Shipping Address"
        titleLabel.font = UIFont.boldSystemFont(ofSize: 22)
        headerView.addSubview(titleLabel)
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
   if indexPath.row == 2 {
    return dynamicHeight
        }
        if indexPath.row == 5 {
            return 120
        }
        return 44
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 6
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "add_firstname", for: indexPath) as? FirstNameTableViewCell
            cell?.firstnameTextField?.tag = indexPath.row
            cell?.firstnameTextField.text = ""
            cell?.firstnameTextField?.delegate = self

            return cell!
        }
        if indexPath.row == 1 {
            guard  let cell = tableView.dequeueReusableCell(withIdentifier: "add_lastname", for: indexPath) as?  LastNameTableViewCell else {
                return UITableViewCell()
            }
            cell.lastnameTextField?.tag = indexPath.row
            cell.lastnameTextField?.delegate = self

           // cell.lastnameTextField.text = ""
            return cell
        }
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "add_address", for: indexPath) as? AddAddressTableViewCell
            cell?.addressTextView?.tag = indexPath.row
            cell?.addressTextView?.delegate = self
            return cell!
        }
        if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "add_city", for: indexPath) as? CityTableViewCell
            cell?.cityTextField?.tag = indexPath.row
            cell?.cityTextField?.delegate = self

           // cell?.cityTextField.text = ""
            return cell!
        }
        if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "add_pincode", for: indexPath) as? PincodeTableViewCell
            cell?.pincodeTextField?.tag = indexPath.row
            cell?.pincodeTextField?.delegate = self
            cell?.pincodeTextField.text = ""
            return cell!
        }
        if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "add_saveaddress", for: indexPath) as? SaveAndcontinueTableViewCell
            cell?.saveAndContinueButton.addTarget(self
                , action: #selector(saveAndContinueButtonAction(_:)), for: .touchUpInside)
            cell?.cancelButton.addTarget(self
                , action: #selector(cancelButtonAction(_:)), for: .touchUpInside)
            return cell!
        }
        // Configure the cell...
        return UITableViewCell()
    }
    
    @objc func saveAndContinueButtonAction(_ sender: AnyObject) {
        let params = ["customer_id":userId,"firstname":firstName,
                      "lastname":lastName,
                      "company":"ui not set",
                      "address_1":address,
                      "address_2":"ui not set",
                      "city":city,
                      "postcode":pincode] 
        let api:String = Constants().qaUrl2 + Constants().addAddressEndPoint
        activityView.startActivity(target: self)       
        addressViewModel.addAddressToMyaccount(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (message, isSuccess) in
            self?.dismiss(animated: false, completion: nil)
            self?.activityView.stopActivity()
            MessageManager.showAlert(title: "Walna Electrics", message: message!, imageName: nil)
        }
    }
    @objc func cancelButtonAction(_ sender: AnyObject) {
       self.dismiss(animated: false, completion: nil)
    }

    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == "Shipping Address *") {
            textView.text = ""
            textView.textColor = .gray
        }
    }
    func textViewDidChange(_ textView: UITextView) {

        let currentOffset = myAddressTableView.contentOffset
        var frame : CGRect = textView.bounds
        frame.size.height = textView.contentSize.height
        textView.bounds = frame
        dynamicHeight = textView.contentSize.height
        UIView.setAnimationsEnabled(false)
        myAddressTableView.beginUpdates()
        myAddressTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        myAddressTableView.setContentOffset(currentOffset, animated: true)
        let thisIndexPath = IndexPath(row: textView.tag, section: 0)
        myAddressTableView?.scrollToRow(at: thisIndexPath as IndexPath, at: .bottom, animated: false)
        if textView.tag == 2 {
            address = textView.text
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Shipping Address *"
            textView.textColor = .lightGray
        }
    }
    //Mark: -Textfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 0 {
            firstName =  textField.text
        }else if textField.tag == 1 {
            lastName = textField.text
        }else if textField.tag == 2 {
            address = textField.text
        }else if textField.tag == 3 {
            city = textField.text
        }else if textField.tag == 4 {
            pincode = textField.text
        }
    }
}
