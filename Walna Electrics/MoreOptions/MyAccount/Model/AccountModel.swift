//
//  AccountModel.swift
//  Walna Electrics
//
//  Created by Admin on 13/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
class ShippingAddress {
    var defaultAddressId:String?
    var addressId:String?
    var firstName:String?
    var lastName:String?
    var company:String?
    var address_1:String?
    var address_2:String?
    var city:String?
    var pinCode:String?
    var zone:String?
    var zone_code:String?
    var country:String?

    init(json : [String: AnyObject]) {
        defaultAddressId = json["default_address_id"] as? String
        addressId = json["address_id"] as? String
        firstName = json["firstname"] as? String
        lastName = json["lastname"] as? String
        company = json["company"] as? String
        address_1 = json["address_1"] as? String
        address_2 = json["address_2"] as? String
        city = json["city"] as? String
        pinCode = json["postcode"] as? String
        zone = json["zone"] as? String
        zone_code = json["zone_code"] as? String
        country = json["country"] as? String
    }
}

