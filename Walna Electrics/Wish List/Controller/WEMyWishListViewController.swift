//
//  WEMyWishListViewController.swift
//  Walna Electrics
//
//  Created by Admin on 22/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
class WEMyWishListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, WEAlertViewDelegate {
    func cancelButtonTapped() {
        
    }
    @IBOutlet weak var wishListTableView: UITableView!
    var wishListViewModel = WishlistViewModel()
    var categoryProductNameName = ""
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var clearAllButton: UIButton!
    @IBOutlet weak var cartCount: UIButton!
    @IBOutlet weak var cartViewButton: UIButton!
    var referenceViewController = UIViewController()
    var activityView = NDActivityViewHelper()
    var weAlertView = WEAlertView.alertViewCustom()


    override func viewWillAppear(_ animated: Bool){
        cartCount.setTitle(cartCountDynamic, for: .normal)
    }
    func setButtonProperties(){
        cartCount.layer.borderWidth = 0.5
        cartCount.layer.cornerRadius = cartCount.frame.size.height / 2
        cartCount.layer.borderColor = UIColor.white.cgColor
    }
        override func viewDidLoad() {
            super.viewDidLoad()
            setButtonProperties()
            getWishlistRequest()
        }
    func getWishlistRequest() {
        // remove all items from cart
        activityView.startActivity(target: self)
        let api:String = Constants().qaUrl2 + Constants().getWishlistEndPoint
        let params = ["customer_id":"3"]
        wishListViewModel.getWishList(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (dict, isSuccess, error ) in
            if(isSuccess)!{
                self?.activityView.stopActivity()
                self?.wishListTableView.delegate = self
                self?.wishListTableView.dataSource = self
                self?.wishListTableView.reloadData()
                let count = dict!["products"] as? [Dictionary<String, Any>]
            }
        }
    }
    @IBAction func cartViewButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WEMyCartViewController") as? WEMyCartViewController
        self.present(viewController!, animated: true)
    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func homeButtonAction(_ sender: Any) {
        showLogin()
    }
    
    @IBAction func clearAllButtonAction(_ sender: Any) {
        weAlertView.frame = self.view.bounds
        weAlertView.setMessage(message: "Sure! \n You want to clear all the items from Wish list")
        weAlertView.delegate = self
        self.view.addSubview(weAlertView)
    }
    func okButtonTapped() {
        weAlertView.removeFromSuperview()
        activityView.startActivity(target: self)
        let params = ["customer_id":"3"]
        print("params :- \(params)")
        let api:String = Constants().qaUrl2 + Constants().clearWishlistEndPoint
        wishListViewModel.removeItemFromWishList(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (dict, isSuccess, error ) in
            if(isSuccess)!{
                print("removed item from cart successfully")
        self?.activityView.stopActivity()
        self?.wishListViewModel.removeAllItemAtCartList()
                self?.dismiss(animated: false, completion: nil)
                MessageManager.showAlert(title: "Walna Electrics", message: "Removed all items from your Wish List successfully", imageName: nil)
                
            }
        }
    }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    }
    extension WEMyWishListViewController {
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 10
        }
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView()
            headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
            headerView.backgroundColor = UIColor.clear
            return headerView
        }

        func numberOfSections(in tableView: UITableView) -> Int {
            return wishListViewModel.allWishListItemsCount()!
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "wishListTableViewCell", for: indexPath) as? ListTableViewCell else {
                fatalError("Cell Doenot exist")
            }
            cell.titleLabel.text = wishListViewModel.wishlist_Description(indexPathCell: indexPath)
            cell.hsnLabel.text = wishListViewModel.wishlist_hsn(indexPathCell: indexPath)
            cell.skuNoLabel.text = wishListViewModel.wishlist_skuNo(indexPathCell: indexPath)
            cell.brandLabel.text = wishListViewModel.wishlist_model(indexPathCell: indexPath)

            cell.thumbImage.imageFromServerURL(urlString: wishListViewModel.wishlist_thumbImageUrl(indexPathCell: indexPath)!)
            cell.priceLabel.text = "₹ " + String(wishListViewModel.wishlist_price(indexPathCell: indexPath)!)
                cell.stockLabel.text = wishListViewModel.wishlist_stockStatus(indexPathCell: indexPath)
            cell.removeItemBtn?.addTarget(self
                , action: #selector(removeItemWishlistAction(_:)), for: .touchUpInside)
            cell.notifyMeBtn?.addTarget(self
                , action: #selector(notifyBtnAction(_:)), for: .touchUpInside)
            return cell
         }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        }
        @objc func removeItemWishlistAction(_ sender: AnyObject) {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.wishListTableView)
            let indexPath = self.wishListTableView.indexPathForRow(at:buttonPosition)
            print("indexPath selected :- \(indexPath?.section)")
            let params = ["cart_id":"\(wishListViewModel.getWishlistId(indexPathCell:(indexPath)!)!)","customer_id":"3"]
            print("params :- \(params)")
            let api:String = Constants().qaUrl2 + Constants().deleteWishlistEndPoint
            wishListViewModel.removeItemFromWishList(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (dict, isSuccess, error ) in
                if(isSuccess)!{
                    print("removed item from cart successfully")
                    self?.wishListViewModel.removeItemAtWishList(indexPathCell: indexPath!)
                    self?.wishListTableView.reloadData()                }
            }
        }
        @objc func notifyBtnAction(_ sender: AnyObject) {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.wishListTableView)
            let indexPath = self.wishListTableView.indexPathForRow(at:buttonPosition)
            print("indexPath selected :- \(indexPath?.section)")
            let params = ["product_id":"\(wishListViewModel.getWishlistId(indexPathCell:(indexPath)!)!)","customer_id":"3","email":"bhaskar.nic.in@gmail.com",
                          "stock_status_id":"5"]
            print("params :- \(params)")
    let api:String = Constants().qaUrl2 + Constants().notifyCustomerWishListEndPoint
    wishListViewModel.notifyMeItemWishList(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (dict, isSuccess, error ) in
                if(isSuccess)!{
                    print("*** Notified item successfully ***")
                }
            }
        }
        
}
