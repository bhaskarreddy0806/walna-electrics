//
//  WishListMode.swift
//  Walna Electrics
//
//  Created by Admin on 28/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
internal class WishListModel {
    let id              :String?
    let name            :String?
    let description      :String?
    let model            :String?
    let catalogue_no     :String?
    let hsn              :String?
    let price            :Int?
    let discount         :String?
    let href             :String?
    let thumb            :String?
    let special          :Int?
    let rating           :Int?
    let dimensions_with_length :String?
    let dimensions       :String?
    let product_option_id:String?
    let stock_status     :String?
    let reviews          :Int?
    init(json : [String: AnyObject]) {
        self.id = json["id"] as? String
        self.name = json["name"] as? String
        self.description = json["description"] as? String
        self.model = json["model"] as? String
        self.catalogue_no = json["catalogue_no"] as? String
        self.hsn = json["hsn"] as? String
        self.price = json["price"] as? Int
        self.discount = json["discount"] as? String
        self.href = json["href"] as? String
        self.thumb = json["thumb"] as? String
        self.special = json["special"] as? Int
        self.rating = json["rating"] as? Int
        self.dimensions_with_length = json["dimensions_with_length"] as? String
        self.dimensions = json["dimensions"] as? String
        self.product_option_id = json["product_option_id"] as? String
        self.stock_status = json["stock_status"] as? String
        self.reviews = json["reviews"] as? Int
    }
}
