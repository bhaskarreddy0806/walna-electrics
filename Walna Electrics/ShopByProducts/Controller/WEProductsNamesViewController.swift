//
//  WEProductsNamesViewController.swift
//  Walna Electrics
//
//  Created by Admin on 23/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEProductsNamesViewController: UIViewController {
    @IBOutlet weak var productNamesTableView: UITableView!
    @IBOutlet weak var navigationTitle: UILabel!
    
    var viewModel = AllProductNamesViewModel()
    var brandName = ""
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .green
        // Do any additional setup after loading the view.
        navigationTitle.text = "Shop By Products"
        let api:String = Constants().qaUrl2 + Constants().getAllProductNamesEndPoint
        viewModel.fetchAllProductNamesList(urlString: api){[weak self] () in
            self?.productNamesTableView.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension WEProductsNamesViewController: UITableViewDelegate, UITableViewDataSource {
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "allProductNames") as! WEBrandBySubbrandsTableViewCell
        cell.backgroundColor = .white
        cell.subBrandName.text =  viewModel.productNameToDisplay(cellIndexPath: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected indexpath.row \(indexPath.section) row")
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WECategoriesByProductViewController") as? WECategoriesByProductViewController
        viewController?.productName = viewModel.productNameToDisplay(cellIndexPath: indexPath)
        self.present(viewController!, animated: true)
    }
}

