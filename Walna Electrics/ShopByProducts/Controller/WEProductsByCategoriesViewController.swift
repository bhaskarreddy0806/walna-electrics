//
//  WEProductsByCategoriesViewController.swift
//  Walna Electrics
//
//  Created by Admin on 23/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEProductsByCategoriesViewController: UIViewController {
    @IBOutlet weak var productsByBrandTableView: UITableView!
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var searchHeightConstraint: NSLayoutConstraint!
    var viewModel = ProductsBySubSubBrandViewModel()
    var cartViewModel = CartViewModel()
    var wishViewModel = WishlistViewModel()
    var needHeightString:Bool = false
    var brandOrCategory_EndPoint = ""
    var cartCountString = ""
    var brandOrCategory_key = ""
    var brandOrCategoryProductName = ""
    @IBOutlet weak var cartCount: UIButton!
    @IBOutlet weak var cartViewButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        cartCount.setTitle(cartCountDynamic, for: .normal)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .green
        if (!needHeightString){
        searchHeightConstraint.constant = 0
        }
        setButtonProperties()
        navigationTitle.text = brandOrCategoryProductName//subBrandName
        self.getCartProductsRequest()
        
        let api:String = Constants().qaUrl2 + brandOrCategory_EndPoint
        viewModel.fetchproductsBySubSubBrandList(urlString: api, method: StringConstant.METHOD_POST, parameters: [brandOrCategory_key:"\(brandOrCategoryProductName)"]){[weak self] () in
            self?.productsByBrandTableView.reloadData()
        }
    }
    //
    @IBAction func cartViewButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WEMyCartViewController") as? WEMyCartViewController
        // viewController?.categoryProductNameName = "sdfdss" //viewModel.categoryNameToDisplay(cellIndexPath: indexPath)
        self.present(viewController!, animated: true)
    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    func setButtonProperties(){
        cartCount.layer.borderWidth = 0.5
        cartCount.layer.cornerRadius = cartCount.frame.size.height / 2
        cartCount.layer.borderColor = UIColor.white.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension WEProductsByCategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 185
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.ProductsCount()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productsListByCategoryProduct") as! WEProductsListByBrandTableViewCell
        cell.backgroundColor = .white
        cell.product_Name.text = viewModel.description(tableIndexPath: indexPath)
        cell.product_HSNid.text = "HSN : " + viewModel.hsnNo(tableIndexPath: indexPath)!
        cell.product_SKUid.text = "SKU : " + viewModel.catalouge_number(tableIndexPath: indexPath)!
        cell.product_price.text = "₹ " + String(viewModel.price(tableIndexPath: indexPath)!)
        cell.product_brand.text = "Brand : " + viewModel.brand(tableIndexPath: indexPath)!
        cell.product_Image.imageFromServerURL(urlString: viewModel.thumbnailImage(tableIndexPath: indexPath)!)
        cell.product_addToCartBtn?.addTarget(self
            , action: #selector(addToCartPressed(_:)), for: .touchUpInside)
        cell.product_favoriteBtn?.addTarget(self
            , action: #selector(addToFavoritePressed(_:)), for: .touchUpInside)
        // Configure the cell...
        return cell
    }
    @objc func addToCartPressed(_ sender: AnyObject) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.productsByBrandTableView)
        let indexPath = self.productsByBrandTableView.indexPathForRow(at:buttonPosition)
        let params = ["product_id":"\(viewModel.productId(tableIndexPath:(indexPath)!)!)","customer_id":"3","quantity":"1","product_option_id":"1917","recurring":""]
        print("params :- \(params)")
        let api:String = Constants().qaUrl2 + Constants().addToCartEndPoint
        cartViewModel.addToCartList(urlString: api, method: StringConstant.METHOD_POST, parameters: params){(dict, isSuccess, error ) in
            if(isSuccess)!{
                print("Item added to cart successfully")
                self.getCartProductsRequest()
            }
        }
    }
    func getCartProductsRequest() {
        let api:String = Constants().qaUrl2 + Constants().getCartProductsEndPoint
        let params = ["customer_id":"3"]
        cartViewModel.getCartProducts(urlString: api, method: StringConstant.METHOD_POST, parameters: params){(dict, isSuccess, error ) in
            if(isSuccess)!{
                let count = dict!["products"] as? [Dictionary<String, Any>]
                cartCountDynamic = (count?.count.description)!
                self.cartCount.setTitle(cartCountDynamic, for: .normal)
            }
        }
    }
    //MARK: - wish List
    func addtoWishlist(indexPathcell: IndexPath) {
        let params = ["product_id":"\(viewModel.productId(tableIndexPath:(indexPathcell))!)","customer_id":"3"]
        print("params :- \(params)")
        let api:String = Constants().qaUrl2 + Constants().addWishlistEndPoint
        wishViewModel.addToWishList(urlString: api, method: StringConstant.METHOD_POST, parameters: params){(dict, isSuccess, error ) in
            if(isSuccess)!{
                print("Item added to Wish list successfully")
            }
        }
    }
    // Add to wish List
    @objc func addToFavoritePressed(_ sender: AnyObject) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.productsByBrandTableView)
        let indexPath = self.productsByBrandTableView.indexPathForRow(at:buttonPosition)
        addtoWishlist(indexPathcell: indexPath!)
    }
    
}
