//
//  WECategoriesByProductViewController.swift
//  Walna Electrics
//
//  Created by Admin on 23/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WECategoriesByProductViewController: UIViewController {
    @IBOutlet weak var categoriesProductsNameTableView: UITableView!
    @IBOutlet weak var navigationTitle: UILabel!
    
    var viewModel = CategoriesByProductsNameViewModel()
    var productName = ""
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationTitle.text = productName
        let api:String = Constants().qaUrl2 + Constants().getCategoriesByProductsNameEndPoint
        viewModel.fetchCategoriesByProductsNameList(urlString: api, method: StringConstant.METHOD_POST, parameters: ["productName":"\(String(describing: productName))"]){[weak self] () in
            self?.categoriesProductsNameTableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension WECategoriesByProductViewController: UITableViewDelegate, UITableViewDataSource {
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoriesByProductsName") as! WEBrandBySubbrandsTableViewCell
        cell.backgroundColor = .white
        cell.subBrandName.text =  viewModel.categoryNameToDisplay(cellIndexPath: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected indexpath.row \(indexPath.section) section")
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WEProductsByCategoriesViewController") as? WEProductsByCategoriesViewController
        viewController?.brandOrCategory_EndPoint = Constants().getProductsByCategoryNameEndPoint
        viewController?.brandOrCategory_key = "categoryName"
        viewController?.brandOrCategoryProductName = viewModel.categoryNameToDisplay(cellIndexPath: indexPath)
        viewController?.needHeightString = true
        self.present(viewController!, animated: true)
    }
}
