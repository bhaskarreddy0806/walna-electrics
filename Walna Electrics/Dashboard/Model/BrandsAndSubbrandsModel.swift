//
//  BrandsAndSubbrands.swift
//  Walna Electrics
//
//  Created by Admin on 15/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

internal class BrandsAndSubbrandsModel {
    let brandName              :String?
    let brandByList            :[Dictionary<String, AnyObject>]
    
    init(json : [String: AnyObject]) {
        //var array = json["brandsAndSubBrands"].arrayValue
        self.brandName = json["brand"] as? String
        self.brandByList = json["subBrands"] as! [Dictionary<String, AnyObject>]
        print("brand name :-\(self.brandName) ***\n brand List :- \(self.brandByList)")
    }
}
internal class SubBrandsByBrandModel {
    let subBrand              :String?
    let image            :String?
    
    init(json : [String: AnyObject]) {
        //var array = json["brandsAndSubBrands"].arrayValue
        self.subBrand = json["subBrand"] as? String
        self.image = json["image"] as? String
    }
}
internal class SubSubBrandsBySubBrandModel {
    let subBrand              :String?
    init(json : [String: AnyObject]) {
        self.subBrand = json["subSubBrandName"] as? String
        print("brand name :-\(self.subBrand) ***\n")
    }
}

