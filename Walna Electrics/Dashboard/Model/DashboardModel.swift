//
//  DashboardModel.swift
//  Walna Electrics
//
//  Created by Admin on 22/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
class ShopByManufacturesModel {
    let title             :String?
    let link              :String?
    let brand_id          :String?
    let image             :String?

    init(json : [String: AnyObject]) {
        //var array = json["brandsAndSubBrands"].arrayValue
        self.title = json["title"] as? String
        self.link = json["link"] as? String
        self.brand_id = json["brand_id"] as? String
        self.image = json["image"] as? String
    }
}
class BannerAutoDisplayModel {
    let title             :String?
    let link              :String?
    let image             :String?
    
    init(json : [String: AnyObject]) {
        self.title = json["title"] as? String
        self.link = json["link"] as? String
        self.image = (json["image"] as? String)?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
}
class ProductsBySubSubBrandModel {
    let catalogue_no:String?
    let description:String?
    let discount:String?
    let ean:String?
    let href:String?
    let hsn:String?
    let jan:String?
    let meta_title:String?
    let model:String?
    let name:String?
    let price:Int?
    let product_id:String?
    let rating:String?
    let special:String?
    let tax:Int?
    let thumb:String?
    let wishedStatus:Bool?

    init(json : [String: AnyObject]) {
        self.catalogue_no   = json["catalogue_no"] as? String
        self.description    = json["description"] as? String
        self.discount       = json["discount"] as? String
        self.ean            = json["ean"] as? String
        self.href           = json["href"] as? String
        self.hsn            = json["hsn"] as? String
        self.jan            = json["jan"] as? String
        self.meta_title     = json["meta_title"] as? String
        self.model          = json["model"] as? String
        self.name           = json["name"] as? String
        self.price          = json["price"] as? Int
        self.product_id     = json["product_id"] as? String
        self.rating         = json["rating"] as? String
        self.special        = json["special"] as? String
        self.tax            = json["tax"] as? Int
        self.thumb          = json["thumb"] as? String
        self.wishedStatus   = false
    }
}
