//
//  BulkOrderTableViewCell.swift
//  Walna Electrics
//
//  Created by Admin on 04/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class BulkOrderTableViewCell: UITableViewCell {
    @IBOutlet weak var sendViaEmail: UIButton!
    @IBOutlet weak var browseByBrands: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        sendViaEmail.backgroundColor = .white
        sendViaEmail.layer.borderWidth = 1.0
        sendViaEmail.layer.borderColor = UIColor.darkGray.cgColor
        
        browseByBrands.backgroundColor = .white
        browseByBrands.layer.borderWidth = 1.0
        browseByBrands.layer.borderColor = UIColor.darkGray.cgColor
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
