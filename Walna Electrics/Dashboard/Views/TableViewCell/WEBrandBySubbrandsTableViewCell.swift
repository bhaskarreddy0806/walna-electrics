//
//  WEBrandBySubbrandsTableViewCell.swift
//  Walna Electrics
//
//  Created by Admin on 18/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEBrandBySubbrandsTableViewCell: UITableViewCell {
    @IBOutlet weak var subBrandName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
