//
//  WEBannerUpdatesTableViewCell.swift
//  Walna Electrics
//
//  Created by Admin on 09/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class WEBannerUpdatesTableViewCell: UITableViewCell {
    var viewModel_bannersAutoDisplay = BannersAutoDisplayViewModel()
    
    var imgsCount:Int = 0
    var animationsFrames:[BannerAutoDisplayModel]?
    @IBOutlet private weak var bannerImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        let bannersListUrl:String = Constants().qaUrl2 + Constants().bannersEndPoint
        viewModel_bannersAutoDisplay.fetchBannersList(urlString: bannersListUrl){[weak self] in
            self?.animationsFrames = self?.viewModel_bannersAutoDisplay.bannersArray()
            if (self?.animationsFrames?.count)! > 0 {
                Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self?.moveToNextImage), userInfo: nil, repeats: true)
            }
        }
    }
    @objc func moveToNextImage() {
        imgsCount = imgsCount + 1
        
        if animationsFrames?.count == imgsCount {
            imgsCount = 0
            bannerImageView.sd_setImage(with: URL(string: (animationsFrames?[imgsCount].image) ?? ""), placeholderImage: UIImage(named:loadImage))
        }else {
            bannerImageView.sd_setImage(with: URL(string: (animationsFrames?[imgsCount].image?.description) ?? "image not available"), placeholderImage: UIImage(named:loadImage))
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    func setBannerImage(with image: UIImage) {
        bannerImageView.image = image
    }
}
