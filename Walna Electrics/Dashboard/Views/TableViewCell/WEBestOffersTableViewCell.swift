//
//  WEBestOffersTableViewCell.swift
//  Walna Electrics
//
//  Created by Admin on 09/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEBestOffersTableViewCell: UITableViewCell {
    @IBOutlet weak var bestOffersCollectionView: UICollectionView!

    override func awakeFromNib() {
    let nib = UINib.init(nibName: "WEBestOffersCollectionViewCell", bundle: Bundle.main)
    bestOffersCollectionView.register(nib, forCellWithReuseIdentifier: "BestOffersCollectionCell")
    
    bestOffersCollectionView.showsHorizontalScrollIndicator = false
    
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    layout.sectionInset = UIEdgeInsets(top: 4, left: 10, bottom: 4, right: 10)
    layout.minimumLineSpacing = 10
    layout.itemSize = CGSize(width: 163, height: 239)
        
    layout.scrollDirection = .horizontal
    bestOffersCollectionView.isUserInteractionEnabled = true
    bestOffersCollectionView.collectionViewLayout = layout
    bestOffersCollectionView.delaysContentTouches = false
    bestOffersCollectionView.reloadData()
    
}

override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
}
func setCollectionViewDataSourceDelegate(dataSourceDelegate delegate: UICollectionViewDelegate & UICollectionViewDataSource, index: NSInteger) {
    bestOffersCollectionView.dataSource = delegate
    bestOffersCollectionView.delegate = delegate
    bestOffersCollectionView.tag = index
    bestOffersCollectionView.reloadData()
}
}
