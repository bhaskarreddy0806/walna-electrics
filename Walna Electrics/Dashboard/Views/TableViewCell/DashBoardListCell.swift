//
//  DashBoardListCell.swift
//  Garage
//
//  Created by Sandeep Lall on 06/12/16.
//  Copyright © 2016 Tarang Software Technologies. All rights reserved.
//

import UIKit

protocol dashBoardListCellDelegate {
    
    func horizontalScrollContentsView( _horizontalScrollContentsView : UICollectionView, numberOfItemsInTableViewIndexPath tableViewIndexPath : IndexPath) -> Int
    
    func horizontalScrollContentsView( _horizontalScrollContentsView : UICollectionView, cellForItemAtContentIndexPath contentIndexPath : IndexPath, inTableViewIndexPath tableViewIndexPath : IndexPath) -> UICollectionViewCell
    
    func horizontalScrollContentsView( _horizontalScrollContentsView : UICollectionView, didSelectItemAtContentIndexPath contentIndexPath : IndexPath, inTableViewIndexPath tableViewIndexPath : IndexPath)
}

class DashBoardListCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {

    var delegate:dashBoardListCellDelegate?
    var horizontalScrollContentsView : UICollectionView?
    var tableViewIndexPath : IndexPath = IndexPath()
    var contentCellSize : CGSize!
    
    
   class func tableView( tableView : UITableView, cellForRowInTableViewIndexPath tableViewIndexPath : IndexPath, withReusableCellIdentifier cellIdentifier: String, delegate object : Any) -> DashBoardListCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as?  DashBoardListCell
        if (cell == nil) {
            cell = DashBoardListCell.init(style:UITableViewCellStyle.default, reuseIdentifier:cellIdentifier)
        }
        cell?.delegate=object as? dashBoardListCellDelegate
        cell?.tableViewIndexPath=tableViewIndexPath
        cell?.horizontalScrollContentsView?.reloadData()
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        self.contentCellSize = CGSize(width:180, height:230)
        let horizontalFlowLayout : UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        horizontalFlowLayout.scrollDirection = .horizontal
        horizontalFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        horizontalFlowLayout.minimumLineSpacing=0
        horizontalFlowLayout.itemSize=CGSize(width:self.contentCellSize.width, height:self.contentCellSize.height)
        
        self.horizontalScrollContentsView = UICollectionView(frame: CGRect(x:self.contentView.bounds.origin.x, y:self.contentView.bounds.origin.y, width:self.contentView.bounds.size.width, height:self.contentCellSize.height), collectionViewLayout: horizontalFlowLayout)
        self.horizontalScrollContentsView?.register(GridCell.self, forCellWithReuseIdentifier: "GridCell")
        self.horizontalScrollContentsView?.backgroundColor=UIColor.clear
        self.horizontalScrollContentsView?.delegate=self
        self.horizontalScrollContentsView?.dataSource=self
        self.horizontalScrollContentsView?.bounces=true
        self.horizontalScrollContentsView?.showsHorizontalScrollIndicator=false
        self.contentView.addSubview(self.horizontalScrollContentsView!)
        return
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func layoutSubviews() {
        let horizontalScrollContentsViewFrameRect : CGRect = CGRect(x:self.contentView.bounds.origin.x, y:self.contentView.bounds.origin.y, width:self.contentView.bounds.size.width, height:self.contentCellSize.height)
        self.horizontalScrollContentsView?.frame = horizontalScrollContentsViewFrameRect
    }
    
    //MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.delegate?.horizontalScrollContentsView(_horizontalScrollContentsView: collectionView, numberOfItemsInTableViewIndexPath: self.tableViewIndexPath))!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return (self.delegate?.horizontalScrollContentsView(_horizontalScrollContentsView: collectionView, cellForItemAtContentIndexPath: indexPath, inTableViewIndexPath: self.tableViewIndexPath))!
    }

    //MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.horizontalScrollContentsView(_horizontalScrollContentsView: collectionView, didSelectItemAtContentIndexPath: indexPath, inTableViewIndexPath: self.tableViewIndexPath)
    }
}
