//
//  WEProductsListByBrandTableViewCell.swift
//  Walna Electrics
//
//  Created by Admin on 18/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEProductsListByBrandTableViewCell: UITableViewCell {
    @IBOutlet weak var product_Image: UIImageView!
    @IBOutlet weak var product_Name: UILabel!
    @IBOutlet weak var product_HSNid: UILabel!
    @IBOutlet weak var product_SKUid: UILabel!
    @IBOutlet weak var product_brand: UILabel!
    @IBOutlet weak var product_price: UILabel!
    @IBOutlet weak var product_favoriteBtn: UIButton!
    @IBOutlet weak var product_addToCartBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setProductInfo() {
        product_Image.image = UIImage(named: "ras.jpg")
        product_Name.text = "Bhaskar reddy"
    }
}
