//
//  MenuCell.swift
//  Garage
//
//  Created by Sandeep Lall on 29/11/16.
//  Copyright © 2016 Tarang Software Technologies. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    var bgView : UIView = UIView()
    var menuImageView : UIImageView = UIImageView()
    var menuLabel : UILabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initialization code
        
        self.bgView.frame=CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD-60, height:50)
        self.bgView.backgroundColor=UIColor.white
        let layer = CALayer()
        layer.frame=CGRect(x:0, y:self.bgView.frame.size.height-1, width:self.bgView.frame.size.width, height:1)
        layer.backgroundColor=UIColor.black.cgColor
        self.bgView.layer.addSublayer(layer)
        self.contentView.addSubview(self.bgView)
        

        self.menuLabel.frame=CGRect(x:50, y:10, width:self.bgView.frame.size.width-60, height:30)
        self.menuLabel.backgroundColor=UIColor.clear
        self.menuLabel.font=UIFont.init(name: "Helvetica", size: 18)
        self.menuLabel.textColor=UIColor.blue
        self.menuLabel.textAlignment=NSTextAlignment.left
        self.menuLabel.numberOfLines=1
        self.menuLabel.adjustsFontSizeToFitWidth=true
        self.menuLabel.contentScaleFactor=0.8
        self.bgView.addSubview(self.menuLabel)
        
        return
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

}
