//
//  ManGridCell.swift
//  Garage
//
//  Created by Sandeep Lall on 06/12/16.
//  Copyright © 2016 Tarang Software Technologies. All rights reserved.
//

import UIKit

class ManGridCell: UICollectionViewCell {

    var bgView : UIView = UIView()
    var gridImgView : UIImageView = UIImageView()
    var gridLabel : UILabel = UILabel()
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        // Initialization code
        
        self.bgView.frame=CGRect(x:5, y:10, width:160, height:120)
     self.bgView.backgroundColor=StringConstant.LIGHTGRAY_COLOR
        self.contentView.addSubview(self.bgView)
        self.gridImgView.frame=CGRect(x:0, y:0, width:self.bgView.frame.size.width, height:self.bgView.frame.size.height)
        self.gridImgView.contentMode = .scaleAspectFill
        self.gridImgView.clipsToBounds=true
        self.bgView.addSubview(self.gridImgView)
    
        return
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

}
