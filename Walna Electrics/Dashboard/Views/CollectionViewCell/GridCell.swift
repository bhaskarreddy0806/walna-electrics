//
//  GridCell.swift
//  Garage
//
//  Created by Sandeep Lall on 06/12/16.
//  Copyright © 2016 Tarang Software Technologies. All rights reserved.
//

import UIKit

class GridCell: UICollectionViewCell {

    var bgView : UIView = UIView()
    var gridImgView : UIImageView = UIImageView()
    var gridLabel : UILabel = UILabel()
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        // Initialization code
        
        self.bgView.frame=CGRect(x:5, y:5, width:170, height:220)
        self.bgView.backgroundColor = .white
        self.bgView.layer.cornerRadius = 5.0
        self.bgView.layer.borderWidth = 1.0
        self.bgView.layer.borderColor = UIColor.lightGray.cgColor
        self.contentView.addSubview(self.bgView)
        
        self.gridImgView.frame=CGRect(x:5 , y:5, width:self.bgView.frame.size.width - 10, height:155)
        self.gridImgView.contentMode = .scaleAspectFit
        self.gridImgView.clipsToBounds=true
        self.bgView.addSubview(self.gridImgView)
        
        self.gridLabel.frame=CGRect(x:5, y:self.gridImgView.frame.maxY, width:self.gridImgView.frame.size.width, height:55)
        self.gridLabel.backgroundColor=UIColor.white
        self.gridLabel.font=UIFont.init(name: "Helvetica", size: 20)
        self.gridLabel.textColor=UIColor.darkGray
        self.gridLabel.textAlignment=NSTextAlignment.left
        self.gridLabel.numberOfLines=2
        self.gridLabel.adjustsFontSizeToFitWidth=false
        self.gridLabel.contentScaleFactor=0.8
        self.bgView.addSubview(self.gridLabel)
        return
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

}
