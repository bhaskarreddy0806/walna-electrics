//
//  WEDropDownView.swift
//  Walna Electrics
//
//  Created by Admin on 14/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
protocol WEDropDownViewDelegate {
    func didSelectOption(indexPath: Int)
}
class WEDropDownView: UIView {
    var dropDownTableView = UITableView(frame: CGRect(x: 0, y: 0, width: 210, height: 250), style: .plain)
    var dropdownViewDelegate: WEDropDownViewDelegate?
    var moreOptionsArray = ["My Account","My Order","Wish List","Customer Support","Logout"]
    var moreImagesArray = ["myAccount","myOrder","fav_unselect","customerSupport","logOut"]

    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)

        let bannerNib = UINib(nibName: "WEDropdownTableViewCell", bundle: nil)
        dropDownTableView.register(bannerNib, forCellReuseIdentifier: "Dropdown")
        dropDownTableView.backgroundColor = .red
        dropDownTableView.delegate = self
        dropDownTableView.dataSource = self
        dropDownTableView.isScrollEnabled = false
        self.addSubview(dropDownTableView)
        dropDownTableView.separatorStyle = .singleLine
        dropDownTableView.reloadData()

    }
    
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }
}
extension WEDropDownView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Dropdown", for: indexPath) as? WEDropdownTableViewCell
        cell?.optionTitle?.text = moreOptionsArray[indexPath.row]
        cell?.iConimageView.image = UIImage(named:moreImagesArray[indexPath.row])
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dropdownViewDelegate?.didSelectOption(indexPath: indexPath.row)
    }
}

