//
//  WEBestOffersCollectionViewCell.swift
//  Walna Electrics
//
//  Created by Admin on 09/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEBestOffersCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var product_Image: UIImageView!
    @IBOutlet private weak var product_Name: UILabel!
    @IBOutlet private weak var product_HSNid: UILabel!
    @IBOutlet private weak var product_SKUid: UILabel!
    @IBOutlet private weak var product_brand: UILabel!
    @IBOutlet private weak var product_price: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setProductImage(with image: UIImage) {
        product_Image.image = image
    }
}
