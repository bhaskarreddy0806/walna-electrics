//
//  WEMainViewController.swift
//
//  Created by Bhaskar Reddy on 12/03/18.
//  Copyright (c) 2015 Bhaskar Reddy. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
internal class WEMainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, dashBoardListCellDelegate, WEManufaturesTableViewCellDelegate, WEAlertViewDelegate  {
    @IBOutlet weak var dashboardTableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var cartCount: UIButton!
    @IBOutlet weak var cartViewButton: UIButton!
    var weAlertView = WEAlertView.alertViewCustom()
    var cellCount: Int?
    var viewModel = DashBoardViewModel()
    var viewModel_manufacturesList = ShopByManufaturesViewModel()
    var menuView = NDMenuView()
    var activityView = NDActivityViewHelper()

    
    var imageArray = [String] ()
    var selectedMenuItem: Int?
    var dropDownViewObj = WEDropDownView()

    override func viewWillAppear(_ animated: Bool) {
        cartCount.setTitle(cartCountDynamic, for: .normal)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("arrived maincontroller")
        imageArray = ["ras.jpg","adar.jpg","isu.jpg","4.jpeg","5.jpeg","6.jpeg","7.jpeg","8.jpeg","9.jpeg","10.jpeg","1.jpeg"]
        dropDownView()
       // apiRequest()
        registerTableViewCell()
        setButtonProperties()
        let brandsAndSubbrands:String = Constants().qaUrl2 + Constants().brandsAndSubBrandsEndPoint
        activityView.startActivity(target: self)
        viewModel.getbrandsAndSubbrands(urlString: brandsAndSubbrands){[weak self] () in
            self?.activityView.stopActivity()
            self?.cellCount = (self?.viewModel.arrayCount())! + 2
            self?.subApisCall()
            self?.dashboardTableView.delegate = self
            self?.dashboardTableView.dataSource = self
            self?.dashboardTableView.bounces = true
            self?.dashboardTableView.reloadData()
        }

    }
    func setButtonProperties(){
        cartCount.layer.borderWidth = 0.5
        cartCount.layer.cornerRadius = cartCount.frame.size.height / 2
        cartCount.layer.borderColor = UIColor.white.cgColor
    }
    func subApisCall(){
        let manufacturesListUrl:String = Constants().qaUrl2 + Constants().shopByManufacturesEndPoint
        viewModel_manufacturesList.fetchShopByManufacturesList(urlString: manufacturesListUrl){[weak self] () in
        self?.dashboardTableView.reloadData()
        }
    }

    func registerTableViewCell() {
        // Do any additional setup after loading the view.
        dashboardTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")

        let bestOffersNib = UINib(nibName: "WEBestOffersTableViewCell", bundle: nil)
        dashboardTableView.register(bestOffersNib, forCellReuseIdentifier: "BestOffers")
        
        let bannerNib = UINib(nibName: "WEBannerUpdatesTableViewCell", bundle: nil)
        dashboardTableView.register(bannerNib, forCellReuseIdentifier: "Banner")
        
    }
    @IBAction func cartViewButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WEMyCartViewController") as? WEMyCartViewController
        self.present(viewController!, animated: true)
    }
    @IBAction func menuTapped(_ sender: Any) {
        menuView.openMenu(target: self)
        menuView.delegate = self
    }

    @IBAction func dropdownTapped(_ sender: Any) {
        if optionsButton.isSelected {
          optionsButton.isSelected = false
          dropDownViewObj.isHidden = false
            
        } else {
            optionsButton.isSelected = true
            dropDownViewObj.isHidden = true
        }
    }
    //MARK: UITableViewDelegate, UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.cellCount!
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 50
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.size.width, height:50)
        headerView.backgroundColor = .white
        
        let orangeLabel = UILabel()
        orangeLabel.frame = CGRect(x:5, y:4, width:7, height:42)
        orangeLabel.backgroundColor = .orange
        headerView.addSubview(orangeLabel)
        
        let titleLabel = UILabel()
        titleLabel.frame = CGRect(x:20, y:8, width:UIScreen.main.bounds.size.width - 25, height:34)
        titleLabel.backgroundColor = .clear
        titleLabel.textColor = .darkGray
        headerView.addSubview(titleLabel)
        
        let borderLine = UILabel()
        borderLine.frame = CGRect(x:4, y:headerView.frame.size.height - 1, width:headerView.frame.size.width - 4, height:1)
        borderLine.backgroundColor = .darkGray
        headerView.addSubview(borderLine)
        print("section value \(section)")
        
        if viewModel.arrayCount() == section - 1{
            orangeLabel.isHidden = true
            titleLabel.text = "Shop by Our Valued Manufactures"
            titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
            borderLine.isHidden = false
            borderLine.frame.size.height = 2
        }
        else {
            titleLabel.text = viewModel.appTitleToDisplay(for: section - 1 )
            titleLabel.font = UIFont.boldSystemFont(ofSize: 27)
            borderLine.isHidden = true

        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 200
        } else if indexPath.section == (viewModel.arrayCount() + 1) {
            return 140
        } else {
            return 230
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            // section 0
            
            let bannerUpdateCell = tableView.dequeueReusableCell(withIdentifier: "Banner") as! WEBannerUpdatesTableViewCell
            bannerUpdateCell.setBannerImage(with: UIImage(named: "load.png")!)
            return bannerUpdateCell

        } else if indexPath.section == (viewModel.arrayCount() + 1) {
            print("indexPath.section: \(indexPath.section)")
            // section last
            let cellIdentifier: String = "CellIdentifier"
            let cell =  WEManufaturesTableViewCell.tableView(tableView : tableView , cellForRowInTableViewIndexPath : indexPath, withReusableCellIdentifier : cellIdentifier, delegate : self)
            cell.horizontalScrollContentsView?.tag = 1
            return cell
        } else {
            // section Middle
        let cellIdentifier: String = "CellIdentifier"
        let cell =  DashBoardListCell.tableView(tableView : tableView , cellForRowInTableViewIndexPath : indexPath, withReusableCellIdentifier : cellIdentifier, delegate : self)
            cell.horizontalScrollContentsView?.tag = 0
        return cell
        }
    }

    //MARK: dashBoardListCellDelegate
    
    func horizontalScrollContentsView( _horizontalScrollContentsView : UICollectionView, numberOfItemsInTableViewIndexPath tableViewIndexPath : IndexPath) -> Int {
        switch _horizontalScrollContentsView.tag {
        case 0:
            return viewModel.numberOfItemsCollectionCellToDisplay(in: tableViewIndexPath)
        case 1:
            return viewModel_manufacturesList.numberOfItemsToDisplay()
        default:
            return 3
        }
    }
    
    func horizontalScrollContentsView( _horizontalScrollContentsView : UICollectionView, cellForItemAtContentIndexPath contentIndexPath : IndexPath, inTableViewIndexPath tableViewIndexPath : IndexPath) -> UICollectionViewCell {
        print("collectionView.tag:** \(_horizontalScrollContentsView.tag)")
        switch _horizontalScrollContentsView.tag {
        case 0:
        let cellIdentifier: String = "GridCell"
        let cell = _horizontalScrollContentsView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: contentIndexPath) as! GridCell
        cell.gridImgView.sd_setImage(with: URL(string: viewModel.productImageToDisplay(tableViewIndexPath: tableViewIndexPath, contentIndexPath: contentIndexPath)), placeholderImage: UIImage(named:"ras.jpg"))

      //  cell.gridImgView.imageFromServerURL(urlString: viewModel.productImageToDisplay(tableViewIndexPath: tableViewIndexPath, contentIndexPath: contentIndexPath))
        let name = viewModel.productNameToDisplay(tableViewIndexPath: tableViewIndexPath, contentIndexPath: contentIndexPath)
        cell.gridLabel.text = name
        return cell
        case 1:
            let cellIdentifier: String = "ManGridCell"
            let cell = _horizontalScrollContentsView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: contentIndexPath) as! ManGridCell
            cell.gridImgView.sd_setImage(with: URL(string: viewModel_manufacturesList.manufactureImageToDisplay(tableViewIndexPath: tableViewIndexPath, collectioncellIndexPath: contentIndexPath)), placeholderImage: UIImage(named:"ras.jpg"))

          //  cell.gridImgView.imageFromServerURL(urlString: viewModel_manufacturesList.manufactureImageToDisplay(tableViewIndexPath: tableViewIndexPath, collectioncellIndexPath: contentIndexPath))
            cell.gridLabel.text = "hhgh" //name
            return cell

        default:
            return UICollectionViewCell()
        }
        return UICollectionViewCell()
    }
    
    func horizontalScrollContentsView( _horizontalScrollContentsView : UICollectionView, didSelectItemAtContentIndexPath contentIndexPath : IndexPath, inTableViewIndexPath tableViewIndexPath : IndexPath) {
        //let name = viewModel.appTitleToDisplay(for: tableViewIndexPath.section - 1)
        if _horizontalScrollContentsView.tag == 0 {
        let subBrandname = viewModel.productNameToDisplay(tableViewIndexPath: tableViewIndexPath, contentIndexPath: contentIndexPath)
        print("didselect name:- \(subBrandname.description)")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WESubbrandsViewController") as! WESubbrandsViewController
            let categoryBrandName = viewModel.appTitleToDisplay(for: tableViewIndexPath.section - 1)
            viewController.categoryBrandName = categoryBrandName
            viewController.subBrandName = subBrandname
            self.present(viewController, animated: false)
        }
    }
}
extension WEMainViewController: WEDropDownViewDelegate {
    func dropDownView() {
        dropDownViewObj.frame = CGRect(x: StringConstant.MAIN_SCREEN_WD - 215, y: 65, width: 210, height: 250)
        dropDownViewObj.backgroundColor = .green
        dropDownViewObj.dropdownViewDelegate = self
        dropDownViewObj.isHidden = true
        self.view.addSubview(dropDownViewObj)
    }
    
    func didSelectOption(indexPath: Int) {
        optionsButton.isSelected = true
        dropDownViewObj.isHidden = true
        if indexPath == 0{
            let storyboard = UIStoryboard(name: "MoreOptions", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"WEMyAccountViewController") as? WEMyAccountViewController
            self.present(viewController!, animated: true)
        }else if indexPath == 1{
            
        }else if indexPath == 2{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"WEMyWishListViewController") as? WEMyWishListViewController
            self.present(viewController!, animated: true)
        }else if indexPath == 3{
        }else if indexPath == 4{
            logoutView()
        }

    }
    //MARK: -Logout features
    func logoutView()
    {
        weAlertView.frame = self.view.bounds
        weAlertView.setMessage(message: "Sure! \n You want to Logout?")
        weAlertView.delegate = self
        self.view.addSubview(weAlertView)
    }
    // MARK: - AlertView delegates
    func okButtonTapped() {
        weAlertView.removeFromSuperview()
    }
    func cancelButtonTapped() {
        
    }
}
