//
//  WESubbrandsViewController.swift
//  Walna Electrics
//
//  Created by Admin on 18/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WESubbrandsViewController: UIViewController {
    @IBOutlet weak var subbrandsTableView: UITableView!
    @IBOutlet weak var navigationTitle: UILabel!
//SubBrandsByBrandViewModel
    var viewModel = SubSubBrandsBySubBrandViewModel()
    var categoryBrandName = ""
    var subBrandName = ""
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .green
        // Do any additional setup after loading the view.
      navigationTitle.text = subBrandName
        let api:String = Constants().qaUrl2 + Constants().getSubSubBrandsBySubBrandNameEndPoint
        viewModel.fetchSubSubbrandsBySubBrand(urlString: api, method: StringConstant.METHOD_POST, parameters: ["subBrandName":"\(String(describing: subBrandName))", "brand":"\(String(describing: categoryBrandName))"]){[weak self] () in
            self?.subbrandsTableView.reloadData()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension WESubbrandsViewController: UITableViewDelegate, UITableViewDataSource {
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfItemsToDisplay()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "brandBySubbrands") as! WEBrandBySubbrandsTableViewCell
        cell.backgroundColor = .white
        cell.subBrandName.text =  viewModel.subBrandNameToDisplay(cellIndexPath: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected indexpath.row \(indexPath.section) row")
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WEProductsByCategoriesViewController") as? WEProductsByCategoriesViewController
        viewController?.brandOrCategory_EndPoint = Constants().getProductsBySubSubBrandsEndPoint
        viewController?.brandOrCategory_key = "subSubBrandName"
        viewController?.brandOrCategoryProductName = viewModel.subBrandNameToDisplay(cellIndexPath: indexPath)
        self.present(viewController!, animated: true)
    }
}
