//
//  WEMainViewExtentionController.swift
//  Walna Electrics
//
//  Created by Admin on 11/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

extension WEMainViewController: NDMenuViewDelegate {
    func menuClosed() {
        
    }
    
    func moveNextController(cellIndex: NSInteger) {
        switch (cellIndex) {
        case 0:
            break
        case 1:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"WEBrandNamesViewController") as! WEBrandNamesViewController
            viewController.brandName = "All Brands"
            self.present(viewController, animated: true)
            break
        case 2:
            let storyboard = UIStoryboard(name: "Products", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"WEProductsNamesViewController") as! WEProductsNamesViewController
            self.present(viewController, animated: true)
            break
        case 3:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"WEBulkOrderViewController") as! WEBulkOrderViewController
            self.present(viewController, animated: true)
            break
        case 4:
            break
        case 5:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"WEDownloadPricelistViewController") as! WEDownloadPricelistViewController
            self.present(viewController, animated: true)
            break
        case 6:

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"WEDownloadCatalougeListViewController") as! WEDownloadCatalougeListViewController
            self.present(viewController, animated: true)
            break

        default:
            print("default")
        }

    }


}
