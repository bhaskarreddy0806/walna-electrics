//
//  WEBulkOrderViewController.swift
//  Walna Electrics
//
//  Created by Admin on 04/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class WEBulkOrderViewController: UIViewController {
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var bulkOrderTableView: UITableView!
 
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let topBorder = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0.5))
        topBorder.backgroundColor = UIColor.black
        navigationBarView.addSubview(topBorder)
        
        let bottomBorder = UIView(frame: CGRect(x: 0, y: navigationBarView.frame.size.height - 1, width: navigationBarView.frame.size.width, height: 0.5))
        bottomBorder.backgroundColor = UIColor.black
        navigationBarView.addSubview(bottomBorder)
    //navigationBarView.layer.shouldRasterize = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//bulkOrder_description
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension WEBulkOrderViewController: UITableViewDelegate, UITableViewDataSource {
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 0
        }
        return 80
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x:0, y:0, width:UIScreen.main.bounds.size.width, height:60)
        headerView.backgroundColor = .white
        
        let titleLabel = UILabel()
        titleLabel.frame = CGRect(x:20, y:8, width:UIScreen.main.bounds.size.width - 25, height:60)
        titleLabel.backgroundColor = .clear
        titleLabel.textColor = .darkGray
        titleLabel.numberOfLines = 2
        titleLabel.lineBreakMode = .byWordWrapping
        headerView.addSubview(titleLabel)
        
        print("section value \(section)")
        
        if section == 0 {
            titleLabel.text = "WE ARE YOUR PARTNERS FOR ALL YOUR ELECTRICAL NEEDS"
            titleLabel.font = UIFont.boldSystemFont(ofSize: 22)
        }
        else if section == 2 {
            titleLabel.text = "Send requirement for the quotation through - via email"
            titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        }
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        if indexPath.section == 2 {
            return 200
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "bulkOrder_description")
            cell?.backgroundColor = .white
            return cell!
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sub_description")
            cell?.backgroundColor = .white
            return cell!
        } else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "requestOrders") as? BulkOrderTableViewCell
        cell?.backgroundColor = .white
            cell?.sendViaEmail?.addTarget(self
                , action: #selector(sendViaEmailAction(_:)), for: .touchUpInside)
            cell?.browseByBrands?.addTarget(self
                , action: #selector(browseByOrdersAction(_:)), for: .touchUpInside)
            return cell!

        }
        return UITableViewCell()
    }

    @objc func sendViaEmailAction(_ sender: AnyObject) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.bulkOrderTableView)
        let indexPath = self.bulkOrderTableView.indexPathForRow(at:buttonPosition)
        print("sendViaEmailAction indexPath selected :- \(indexPath?.section)")
        share()
    }
    @objc func browseByOrdersAction(_ sender: AnyObject) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.bulkOrderTableView)
        let indexPath = self.bulkOrderTableView.indexPathForRow(at:buttonPosition)
        print("browseByOrdersAction indexPath selected :- \(indexPath?.section)")
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected indexpath.row \(indexPath.section) row")
       
    }
    func share() {
        let activityViewController = UIActivityViewController(
            activityItems: ["Check out walna Electricts ", "http://www.walna.in/"],
            applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }

}

