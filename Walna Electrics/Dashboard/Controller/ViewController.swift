//
//  ViewController.swift
//  Garage
//
//  Created by Sandeep Lall on 17/11/16.
//  Copyright © 2016 Tarang Software Technologies. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var splashImgView: UIImageView!
    weak var timer:Timer?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.splashImgView.backgroundColor=StringConstant.LIGHTGRAY_COLOR
        
       // timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
        print("*********** OUt ******************************************************")

        timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { [weak self] timer in
            print("*********** In Timer ******************************************************")
            self?.update()
        }
    }
    deinit {
        timer?.invalidate()
    }
    @objc func update() {
        // Something cool
        print("*****************************************************************")
        DispatchQueue.main.async {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WEMainViewController") as? WEMainViewController
        self.present(viewController!, animated: false)
        }
        print("********navMenu *********************************************************")
      

       // showLogin()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

  
    }
    
  
    
    func showReg(){
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

 extension UIViewController {
 func showLogin(){
 /*let homeViewController = StringConstant.kMainStoryboard.instantiateViewController(withIdentifier: "Main") as! WEMainViewController
 let navMenu = UINavigationController.init(rootViewController: homeViewController)
 navMenu.isNavigationBarHidden = true
 self.present(navMenu, animated: false, completion: nil)
 */
 }
 
 }
