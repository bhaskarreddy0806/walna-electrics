//
//  UIColor+Helper.swift
//  MLM
//
//  Created by Rajath on 22/07/17.
//  Copyright © 2017 TDB. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func themeGreenColor() -> UIColor {
        return #colorLiteral(red: 0.01176470588, green: 0.4, blue: 0.1450980392, alpha: 1)
    }
    
    static func themeLightGray() -> UIColor {
        return #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
    }
    
    static func themeSelectedText() -> UIColor {
        return #colorLiteral(red: 0.05098039216, green: 0.2980392157, blue: 0.1215686275, alpha: 1)
    }
    
    static func themeUnselectedText() -> UIColor {
        return #colorLiteral(red: 0.3529411765, green: 0.5098039216, blue: 0.1411764706, alpha: 1)
    }
    
    static func themeYelloBackground() -> UIColor {
        return #colorLiteral(red: 0.9921568627, green: 0.8509803922, blue: 0.02745098039, alpha: 1)
    }
    
  /*  convenience init(hexString: String) {
        var hex: String = hexString
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            hex = hexString.substring(from: start)
        }
        
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    */
    
}
