//
//  FBLoader.swift
//  FBAnimatedView
//
//  Created by Samhan on 08/01/16.
//  Copyright © 2016 Samhan. All rights reserved.
//

import UIKit

@objc public protocol ListLoadable {
    func ld_visibleContentViews() -> [UIView]
}

extension UITableView: ListLoadable {
    public func ld_visibleContentViews() -> [UIView] {
        return (visibleCells as NSArray).value(forKey: "contentView") as! [UIView]
    }
}

extension UICollectionView: ListLoadable {
    public func ld_visibleContentViews() -> [UIView] {
        return (visibleCells as NSArray).value(forKey: "contentView") as! [UIView]
    }
}

extension UIColor {
    
    static func backgroundFadedGrey() -> UIColor {
        return UIColor(red: 0.9647, green: 0.9686, blue: 0.9725, alpha: 1)
    }
    
    static func gradientFirstStop() -> UIColor {
        return UIColor(red: 0.9333, green: 0.9333, blue: 0.9333, alpha: 1.0)
    }
    
    static func gradientSecondStop() -> UIColor {
        return UIColor(red: 0.8667, green: 0.8667, blue: 0.8667, alpha: 1.0)
    }
}

extension UIView {
    func boundInside(_ superView: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        superView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views:["subview": self]))
        superView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views:["subview": self]))
    }
}

internal class Loader {
    static func addLoaderToViews(_ views: [UIView]) {
        CATransaction.begin()
        views.forEach { $0.ld_addLoader() }
        CATransaction.commit()
    }
    
    static func removeLoaderFromViews(_ views: [UIView]) {
        CATransaction.begin()
        views.forEach { $0.ld_removeLoader() }
        CATransaction.commit()
    }
    
    open static func addLoaderTo(_ list: ListLoadable) {
        addLoaderToViews(list.ld_visibleContentViews())
    }
    
    open static func removeLoaderFrom(_ list: ListLoadable) {
        removeLoaderFromViews(list.ld_visibleContentViews())
    }
}

internal class CutoutView: UIView {
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(UIColor.white.cgColor)
        context?.fill(bounds)
        if let mainview = superview?.subviews {
            for view in mainview where view != self {
                context?.setBlendMode(.clear)
                context?.setFillColor(UIColor.clear.cgColor)
                context?.fill(view.frame)
            }
        }
    }
    
    override func layoutSubviews() {
        setNeedsDisplay()
        if let mainBounds = superview?.bounds {
            superview?.ld_getGradient()?.frame = mainBounds
        }
    }
}

internal var cutoutHandle: UInt8 = 0
internal var gradientHandle: UInt8 = 0
internal var loaderDuration = 0.85
internal var gradientWidth = 0.17
internal var gradientFirstStop = 0.1

extension CGFloat {
    func doubleValue() -> Double {
        return Double(self)
    }
}

extension UIView {
    
    public func ld_getCutoutView() -> UIView? {
        return objc_getAssociatedObject(self, &cutoutHandle) as! UIView?
    }
    
    func ld_setCutoutView(_ aView: UIView) {
        return objc_setAssociatedObject(self, &cutoutHandle, aView, .OBJC_ASSOCIATION_RETAIN)
    }
    
    func ld_getGradient() -> CAGradientLayer? {
        return objc_getAssociatedObject(self, &gradientHandle) as! CAGradientLayer?
    }
    
    func ld_setGradient(_ aLayer: CAGradientLayer) {
        return objc_setAssociatedObject(self, &gradientHandle, aLayer, .OBJC_ASSOCIATION_RETAIN)
    }
    
    public func ld_addLoader() {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height)
        layer.insertSublayer(gradient, at:0)
        configureAndAddAnimationToGradient(gradient)
        addCutoutView()
    }
    
    public func ld_removeLoader() {
        ld_getCutoutView()?.removeFromSuperview()
        ld_getGradient()?.removeAllAnimations()
        ld_getGradient()?.removeFromSuperlayer()
        
        for view in subviews {
            view.alpha = 1
        }
    }
    
    func configureAndAddAnimationToGradient(_ gradient: CAGradientLayer) {
        gradient.startPoint = CGPoint(x: -1.0 + CGFloat(gradientWidth), y: 0)
        gradient.endPoint = CGPoint(x: 1.0 + CGFloat(gradientWidth), y: 0)
        gradient.colors = [
            UIColor.backgroundFadedGrey().cgColor,
            UIColor.gradientFirstStop().cgColor,
            UIColor.gradientSecondStop().cgColor,
            UIColor.gradientFirstStop().cgColor,
            UIColor.backgroundFadedGrey().cgColor
        ]
        let startLocations = [NSNumber(value: gradient.startPoint.x.doubleValue() as Double),
                              NSNumber(value: gradient.startPoint.x.doubleValue() as Double),
                              NSNumber(value: 0 as Double),
                              NSNumber(value: gradientWidth as Double),
                              NSNumber(value: 1 + gradientWidth as Double)]
        gradient.locations = startLocations
        let gradientAnimation = CABasicAnimation(keyPath: "locations")
        gradientAnimation.fromValue = startLocations
        gradientAnimation.toValue = [NSNumber(value: 0 as Double),
                                     NSNumber(value: 1 as Double),
                                     NSNumber(value: 1 as Double),
                                     NSNumber(value: 1 + (gradientWidth - gradientFirstStop) as Double),
                                     NSNumber(value: 1 + gradientWidth as Double)]
        gradientAnimation.repeatCount = Float.infinity
        gradientAnimation.fillMode = kCAFillModeForwards
        gradientAnimation.isRemovedOnCompletion = false
        gradientAnimation.duration = loaderDuration
        gradient.add(gradientAnimation, forKey: "locations")
        ld_setGradient(gradient)
    }
    
    func addCutoutView() {
        let cutout = CutoutView()
        cutout.frame = bounds
        cutout.backgroundColor = UIColor.clear
        addSubview(cutout)
        cutout.setNeedsDisplay()
        cutout.boundInside(self)
        for view in subviews where view != cutout {
            view.alpha = 0
        }
        ld_setCutoutView(cutout)
    }
}
