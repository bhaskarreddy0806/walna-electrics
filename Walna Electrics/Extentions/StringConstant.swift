//
//  StringConstant.swift
//  Swift Demo
//
//  Created by Sandeep Lall on 22/09/16.
//  Copyright © 2016 Tarang Software Technologies. All rights reserved.
//

import UIKit

class StringConstant: NSObject {

    static let METHOD_POST = "POST"
    static let PLACE_HOLDER_IMAGE = UIImage.init(named: "adImage")

    static let kMainStoryboard = UIStoryboard(name: "Main", bundle: nil)

    static let MAIN_SCREEN_WD = UIScreen.main.bounds.size.width
    static let MAIN_SCREEN_HT = UIScreen.main.bounds.size.height
    
    static let DEVICE_VERSION = UIDevice.current.systemVersion
    
    static let GRID_WD = (MAIN_SCREEN_WD/3)
    static let GRID_HT = 120
    static let AD_IMAGE_HT = (MAIN_SCREEN_HT/3.43) // 480/3.43 = 140

    static let ORANGE_COLOR = UIColor(hexString: "#FF5F40")
    static let LIGHTGRAY_COLOR = UIColor(hexString: "#CDCDCD")
    static let BGTABLEVIEW_COLOR = UIColor(hexString: "#EBEBEB")


}

