
//
//  DashBoardViewModel.swift
//  
//
//  Created by Admin on 03/22/18.
//  Copyright © 2017 Erica Millado. All rights reserved.
//

import UIKit
import SwiftyJSON
class DashBoardViewModel {
    var apiClient = APIClient()

    var brandsAndSubbrandsArray:[BrandsAndSubbrandsModel] = [BrandsAndSubbrandsModel]()

    // - This function is what directly accesses the apiClient to make the API call
    func getbrandsAndSubbrands(urlString: String, completion: @escaping () -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.getRequestFetch(urlString: urlString, completion: {[weak self] (arrayOfAppsDictionaries ) in
            DispatchQueue.main.async {
        //7 - assign our local apps array to our returned json
                print(arrayOfAppsDictionaries!)
                guard let array = arrayOfAppsDictionaries else {
                    
                    return
                }
                for item in (array) {
                    print(item)
                    let value = BrandsAndSubbrandsModel.init(json:item as [String : AnyObject])
                    self?.brandsAndSubbrandsArray.append(value)
                }
                completion()
            }
        })
    }
    // MARK: - values to display in our table view controller
    func arrayCount() -> Int {
        return brandsAndSubbrandsArray.count
    }
    func numberOfItemsToDisplay(in section: Int) -> Int {
        return brandsAndSubbrandsArray.count
    }
    // MARK: - display title on Sections header
    func appTitleToDisplay(for indexPath: Int) -> String {
        return brandsAndSubbrandsArray[indexPath].brandName ?? ""
    }
    // MARK: - count for Collectioncell numberOfItems
    func numberOfItemsCollectionCellToDisplay(in numberOfCollectionViewRowsIndex: IndexPath) -> Int {
        return brandsAndSubbrandsArray[numberOfCollectionViewRowsIndex.section - 1].brandByList.count
    }
    // MARK: - image and name  on CollectionviewCell
    func productNameToDisplay(tableViewIndexPath: IndexPath, contentIndexPath: IndexPath) -> String {
        return (brandsAndSubbrandsArray[tableViewIndexPath.section - 1].brandByList[contentIndexPath.row]["subBrand"] as? String)!
    }
    func productImageToDisplay(tableViewIndexPath: IndexPath, contentIndexPath: IndexPath) -> String {
        return (brandsAndSubbrandsArray[tableViewIndexPath.section - 1].brandByList[contentIndexPath.row]["image"] as? String)!
    }
    // MARK: - didSelectItem at CollectionviewCell
    func collectionCell_didSelectItem(tableViewIndexPath: IndexPath, contentIndexPath: IndexPath) -> String {
        return  brandsAndSubbrandsArray[tableViewIndexPath.section - 1].brandByList[contentIndexPath.row]["subBrand"] as? String ?? ""
    }
}

class SubSubBrandsBySubBrandViewModel {
    var apiClient = APIClient()
    var subSubBrandsBySubBrandArray:[SubSubBrandsBySubBrandModel] = [SubSubBrandsBySubBrandModel]()
    func fetchSubSubbrandsBySubBrand(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping () -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.postRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (arrayOfAppsDictionaries ) in
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                print(arrayOfAppsDictionaries!)
                for item in (arrayOfAppsDictionaries)! {
                    print(item)
                    let value = SubSubBrandsBySubBrandModel.init(json:item as [String : AnyObject])
                    self?.subSubBrandsBySubBrandArray.append(value)
                }
                completion()
            }
        })
    }
    // MARK: - count for Tableview numberOfItems
    func numberOfItemsToDisplay() -> Int {
        return subSubBrandsBySubBrandArray.count
    }
    // MARK: - values to display in our table view cell
    func subBrandNameToDisplay(cellIndexPath: IndexPath) -> String {
        return subSubBrandsBySubBrandArray[cellIndexPath.section].subBrand ?? ""
    }
}
class SubBrandsByBrandViewModel {
    var apiClient = APIClient()
    var subBrandsByBrandArray:[SubBrandsByBrandModel] = [SubBrandsByBrandModel]()
    
    // - This function is what directly accesses the apiClient to make the API call
    func fetchSubbrandsByBrand(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping () -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.postRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (arrayOfAppsDictionaries ) in
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                print(arrayOfAppsDictionaries!)
                for item in (arrayOfAppsDictionaries)! {
                    print(item)
                    let value = SubBrandsByBrandModel.init(json:item as [String : AnyObject])
                    self?.subBrandsByBrandArray.append(value)
                }
                completion()
            }
        })
    }
    // MARK: - count for Tableview numberOfItems
    func numberOfItemsToDisplay() -> Int {
        return subBrandsByBrandArray.count
    }
    // MARK: - values to display in our table view cell
    func subBrandNameToDisplay(cellIndexPath: IndexPath) -> String {
        return subBrandsByBrandArray[cellIndexPath.section].subBrand ?? ""
    }
    func subBrandImage(cellIndexPath: IndexPath) -> String {
        return subBrandsByBrandArray[cellIndexPath.section].image ?? ""
    }

    
}
class ShopByManufaturesViewModel {
    var apiClient = APIClient()
    var shopByManufacturesArray:[ShopByManufacturesModel] = [ShopByManufacturesModel]()
    
    // - This function is what directly accesses the apiClient to make the API call
    func fetchShopByManufacturesList(urlString: String, completion: @escaping () -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.getRequestFetch(urlString: urlString, completion: {[weak self] (arrayOfAppsDictionaries ) in

            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                print(arrayOfAppsDictionaries!)
                for item in (arrayOfAppsDictionaries)! {
                    print(item)
                    let value = ShopByManufacturesModel.init(json:item as [String : AnyObject])
                    self?.shopByManufacturesArray.append(value)
                }
                completion()
            }
        })
    }
    // MARK: - count for ShopBy Manufactures
    func numberOfItemsToDisplay() -> Int {
        return shopByManufacturesArray.count
    }
    // MARK: - values to display in our CollectionViewCell cell
    func manufactureImageToDisplay(tableViewIndexPath: IndexPath, collectioncellIndexPath: IndexPath) -> String {
        print("collectioncellIndexPath.row :\(collectioncellIndexPath.row)")
        let imageUrl = shopByManufacturesArray[collectioncellIndexPath.row].image
        let filterPercentageimageUrl = imageUrl?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        return filterPercentageimageUrl ?? ""
       // return shopByManufacturesArray[collectioncellIndexPath.row].image ?? ""
    }
}
class BannersAutoDisplayViewModel {
    var apiClient = APIClient()
    var bannersImagesArray:[BannerAutoDisplayModel] = [BannerAutoDisplayModel]()
    
    // - This function is what directly accesses the apiClient to make the API call
    func fetchBannersList(urlString: String, completion: @escaping () -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.getRequestFetch_BannersAutoDisplay(urlString: urlString, completion: {[weak self] (arrayOfAppsDictionaries, isSuccess, error) in
            
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                print(arrayOfAppsDictionaries!)
                for item in (arrayOfAppsDictionaries)! {
                    print(item)
                    let value = BannerAutoDisplayModel.init(json:item as [String : AnyObject])
                    self?.bannersImagesArray.append(value)
                }
                print("self.bannersImagesArray :-- \(self?.bannersImagesArray)")
                completion()
            }
        })
    }
    // MARK: - banners data array
    func bannersArray() -> [BannerAutoDisplayModel]{
        return bannersImagesArray 
    }

}
class ProductsBySubSubBrandViewModel {
    var apiClient = APIClient()
    var productsBySubSubBrandArray:[ProductsBySubSubBrandModel] = [ProductsBySubSubBrandModel]()
    
    // - This function is what directly accesses the apiClient to make the API call
    func fetchproductsBySubSubBrandList(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping () -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.postRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (arrayOfAppsDictionaries ) in
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                print(arrayOfAppsDictionaries!)
                for item in (arrayOfAppsDictionaries)! {
                    print(item)
                    let value = ProductsBySubSubBrandModel.init(json:item as [String : AnyObject])
                    self?.productsBySubSubBrandArray.append(value)
                }
                print("productsBySubSubBrandArray :-- \(self?.productsBySubSubBrandArray)")
                completion()
            }
        })
    }
    // MARK: - banners data array
    func productsArray() -> [ProductsBySubSubBrandModel]{
        return productsBySubSubBrandArray
    }
    func ProductsCount() -> Int{
        return productsBySubSubBrandArray.count
    }
    func description(tableIndexPath: IndexPath) -> String? {
        return productsBySubSubBrandArray[tableIndexPath.section].description ?? ""
    }
    func price(tableIndexPath: IndexPath) -> Int? {
        return productsBySubSubBrandArray[tableIndexPath.section].price ?? 0
    }
    func productId(tableIndexPath: IndexPath) -> String? {
        return productsBySubSubBrandArray[tableIndexPath.section].product_id ?? ""
    }
    func hsnNo(tableIndexPath: IndexPath) -> String? {
        return productsBySubSubBrandArray[tableIndexPath.section].hsn ?? ""
    }
    func rating(tableIndexPath: IndexPath) -> String? {
        return productsBySubSubBrandArray[tableIndexPath.section].rating ?? ""
    }
    func thumbnailImage(tableIndexPath: IndexPath) -> String? {
        return productsBySubSubBrandArray[tableIndexPath.section].thumb ?? ""
    }
    func discount(tableIndexPath: IndexPath) -> String? {
        return productsBySubSubBrandArray[tableIndexPath.section].discount ?? ""
    }
    func tax(tableIndexPath: IndexPath) -> Int? {
        return productsBySubSubBrandArray[tableIndexPath.section].tax ?? 0
    }
    func catalouge_number(tableIndexPath: IndexPath) -> String? {
        return productsBySubSubBrandArray[tableIndexPath.section].catalogue_no ?? ""
    }
    func href(tableIndexPath: IndexPath) -> String? {
        return productsBySubSubBrandArray[tableIndexPath.section].href ?? ""
    }
    func jan(tableIndexPath: IndexPath) -> String? {
        return productsBySubSubBrandArray[tableIndexPath.section].jan ?? ""
    }
    func brand(tableIndexPath: IndexPath) -> String? {
        return productsBySubSubBrandArray[tableIndexPath.section].model ?? ""
    }
}
