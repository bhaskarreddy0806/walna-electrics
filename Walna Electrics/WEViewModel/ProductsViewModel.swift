//
//  ProductsViewModel.swift
//  Walna Electrics
//
//  Created by Admin on 23/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON
class AllProductNamesViewModel {
        var apiClient = APIClient()
        var allProductNamesArray:[AllProductNamesModel] = [AllProductNamesModel]()
        
        // - This function is what directly accesses the apiClient to make the API call
        func fetchAllProductNamesList(urlString: String, completion: @escaping () -> Void) {
            //5 - call on the apiClient to fetch the brands and Subbrands
            apiClient.getRequestFetch(urlString: urlString, completion: {[weak self] (arrayOfAppsDictionaries ) in
                
                DispatchQueue.main.async {
                    //7 - assign our local apps array to our returned json
                    print(arrayOfAppsDictionaries!)
                    for item in (arrayOfAppsDictionaries)! {
                        print(item)
                        let value = AllProductNamesModel.init(json:item as [String : AnyObject])
                        self?.allProductNamesArray.append(value)
                    }
                    print("allBrandNamesArray :-- \(self?.allProductNamesArray)")
                    completion()
                }
            })
        }
        // MARK: - banners data array
        func numberOfSections() -> Int{
            return allProductNamesArray.count
        }
        // MARK: - values to display in our table view cell
        func productNameToDisplay(cellIndexPath: IndexPath) -> String {
            return allProductNamesArray[cellIndexPath.section].name ?? ""
        }
}
class CategoriesByProductsNameViewModel {
    var apiClient = APIClient()
    var allCategoriesProductNamesArray:[CategoriesByProductsNameModel] = [CategoriesByProductsNameModel]()
    
    // - This function is what directly accesses the apiClient to make the API call
    func fetchCategoriesByProductsNameList(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping () -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.postRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (arrayOfAppsDictionaries ) in
            
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                print(arrayOfAppsDictionaries!)
                for item in (arrayOfAppsDictionaries)! {
                    print(item)
                    let value = CategoriesByProductsNameModel.init(json:item as [String : AnyObject])
                    self?.allCategoriesProductNamesArray.append(value)
                }
                print("allCategoriesProductNamesArray :-- \(self?.allCategoriesProductNamesArray)")
                completion()
            }
        })
    }
    // MARK: - banners data array
    func numberOfSections() -> Int{
        return allCategoriesProductNamesArray.count
    }
    // MARK: - values to display in our table view cell
    func categoryNameToDisplay(cellIndexPath: IndexPath) -> String {
        return allCategoriesProductNamesArray[cellIndexPath.section].name ?? ""
    }
}
//add to Cart

class CartViewModel {
    var apiClient = APIClient()
    var allCartProductsListArray:[CartListModel] = [CartListModel]()
    func addToCartList(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping (Dictionary<String, Any>?, _ isSuccess: Bool?,
    _ error: String?) -> Void) {
    //5 - call on the apiClient to fetch the brands and Subbrands
    apiClient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error ) in
        
        DispatchQueue.main.async {
            //7 - assign our local apps array to our returned json
            if let dict = dict {
                print("cart success :-- \(dict)")
            }
            completion(dict, isSuccess, error)
        }
    })
}
    // Get All Cart products
    func getCartProducts(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping (Dictionary<String, Any>?, _ isSuccess: Bool?,
        _ error: String?) -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error ) in
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                if let dict = dict {
                    print("get cart Products :-- \(dict)")
                    let allCartProducts = dict["products"] as? [Dictionary<String, Any>]
                    for item in (allCartProducts)! {
                        print(item)
                        let value = CartListModel.init(json:item as [String : AnyObject])
                        self?.allCartProductsListArray.append(value)
                    }
                }
                completion(dict, isSuccess, error)
            }
        })
    }
    func removeAllItemAtCartList() {
    allCartProductsListArray.removeAll()
    }
    func getTotalCartPrice() -> String{
        let sum = allCartProductsListArray.reduce(0, { $0 + $1.price! })
        return "Total ₹ " + sum.description
    }
    func allCartItemsCount() -> Int? {
        return allCartProductsListArray.count ?? 0
    }
    func removeItemAtCartList(indexPathCell: IndexPath) {
        allCartProductsListArray.remove(at: indexPathCell.section)
    }
    func getCartId(indexPathCell: IndexPath) -> String? {
        return allCartProductsListArray[indexPathCell.section].cart_id
    }
    func cartDescription(indexPathCell: IndexPath) -> String? {
        return allCartProductsListArray[indexPathCell.section].description
    }
    func cart_hsn(indexPathCell: IndexPath) -> String? {
        return allCartProductsListArray[indexPathCell.section].hsn
    }
    func cart_price(indexPathCell: IndexPath) -> Int? {
        return allCartProductsListArray[indexPathCell.section].price
    }
    func cart_model(indexPathCell: IndexPath) -> String? {
        return allCartProductsListArray[indexPathCell.section].model
    }
    func cart_skuNo(indexPathCell: IndexPath) -> String? {
        return allCartProductsListArray[indexPathCell.section].catalogue_no
    }
    func cart_quantityNo(indexPathCell: IndexPath) -> String? {
        return allCartProductsListArray[indexPathCell.section].quantity
    }
    func cart_isStock(indexPathCell: IndexPath) -> String? {
        return allCartProductsListArray[indexPathCell.section].stock
    }
    func cart_thumbImageUrl(indexPathCell: IndexPath) -> String? {
    let imageUrl = allCartProductsListArray[indexPathCell.section].thumb
    let filterPercentageimageUrl = imageUrl?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    return filterPercentageimageUrl ?? ""
    }
    func removeItemFromCartList(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping (Dictionary<String, Any>?, _ isSuccess: Bool?,
        _ error: String?) -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error ) in
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                if let dict = dict {
                    print("remove item cart:-- \(dict)")
                }
                completion(dict, isSuccess, error)
            }
        })
    }
// MARK: - banners data array
}


