//
//  WishLisViewModel.swift
//  Walna Electrics
//
//  Created by Admin on 28/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
class WishlistViewModel {
    var apiClient = APIClient()
    var allWishListArray:[WishListModel] = [WishListModel]()
    
    // Add to Wish list
    func addToWishList(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping (Dictionary<String, Any>?, _ isSuccess: Bool?,
        _ error: String?) -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error ) in
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                if let dict = dict {
                    print("cart success :-- \(dict)")
                }
                completion(dict, isSuccess, error)
            }
        })
    }
    func removeItemFromWishList(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping (Dictionary<String, Any>?, _ isSuccess: Bool?,
        _ error: String?) -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error ) in
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                if let dict = dict {
                    print("remove item cart:-- \(dict)")
                }
                completion(dict, isSuccess, error)
            }
        })
    }
    func notifyMeItemWishList(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping (Dictionary<String, Any>?, _ isSuccess: Bool?,
        _ error: String?) -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: { [weak self] (dict, isSuccess, error ) in
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                if let dict = dict {
                    print("remove item cart:-- \(dict)")
                }
                completion(dict, isSuccess, error)
            }
        })
    }
    
    func getWishList(urlString: String?, method: String, parameters:[String: Any]?, completion: @escaping (Dictionary<String, Any>?, _ isSuccess: Bool?,
        _ error: String?) -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error ) in
            
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                if let dict = dict {
                    let allCartProducts = dict["wishlist"] as? [Dictionary<String, Any>]
                    for item in (allCartProducts)! {
                        print(item)
                        let value = WishListModel.init(json:item as [String : AnyObject])
                        self?.allWishListArray.append(value)
                    }
                    print("allWishProductsListArray :-- \(self?.allWishListArray)")
                }
                completion(dict, isSuccess, error)
            }
        })
    }
    func removeAllItemAtCartList() {
        allWishListArray.removeAll()
    }
    func allWishListItemsCount() -> Int? {
        return allWishListArray.count ?? 0
    }
    func removeItemAtWishList(indexPathCell: IndexPath) {
        allWishListArray.remove(at: indexPathCell.section)
    }
    func getWishlistId(indexPathCell: IndexPath) -> String? {
        return allWishListArray[indexPathCell.section].id
    }
    func wishlist_name(indexPathCell: IndexPath) -> String? {
        return allWishListArray[indexPathCell.section].name
    }
    func wishlist_Description(indexPathCell: IndexPath) -> String? {
        return allWishListArray[indexPathCell.section].description
    }
    func wishlist_model(indexPathCell: IndexPath) -> String? {
        return allWishListArray[indexPathCell.section].model
    }
    func wishlist_skuNo(indexPathCell: IndexPath) -> String? {
        return allWishListArray[indexPathCell.section].catalogue_no
    }
    func wishlist_hsn(indexPathCell: IndexPath) -> String? {
        return allWishListArray[indexPathCell.section].hsn
    }
    func wishlist_price(indexPathCell: IndexPath) -> Int? {
        return allWishListArray[indexPathCell.section].price
    }
    func wishlist_special(indexPathCell: IndexPath) -> Int? {
        return allWishListArray[indexPathCell.section].special
    }
    func wishlist_rating(indexPathCell: IndexPath) -> Int? {
        return allWishListArray[indexPathCell.section].rating
    }
    func wishlist_reviews(indexPathCell: IndexPath) -> Int? {
        return allWishListArray[indexPathCell.section].reviews
    }
    func wishlist_stockStatus(indexPathCell: IndexPath) -> String? {
        return allWishListArray[indexPathCell.section].stock_status
    }
    func wishlist_thumbImageUrl(indexPathCell: IndexPath) -> String? {
        let imageUrl = allWishListArray[indexPathCell.section].thumb
        let filterPercentageimageUrl = imageUrl?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        return filterPercentageimageUrl ?? ""
    }
}
