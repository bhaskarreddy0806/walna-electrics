//
//  MyAccountViewModel.swift
//  Walna Electrics
//
//  Created by Admin on 13/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class AddressViewModel {
    var apiCient = APIClient()
    var addressesArray:[ShippingAddress] = [ShippingAddress]()

    func updatePhoneNumberToMyaccount(urlString: String?, method: String, parameters:[String: Any]?, completion:@escaping (String?, Bool) ->Void){
        apiCient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error) in
            
            completion(dict?["telephone"] as? String, isSuccess!)
        })
    }
    func addAddressToMyaccount(urlString: String?, method: String, parameters:[String: Any]?, completion:@escaping (String?, Bool) ->Void){
        apiCient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error) in
            completion(dict?["success"] as? String, isSuccess!)
        })
    }
    func deleteAddressFromMyaccount(urlString: String?, method: String, parameters:[String: Any]?, completion:@escaping (String?, Bool) ->Void){
        apiCient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error) in
            completion(dict?["success"] as? String, isSuccess!)
        })
    }
    func setDefaultAddressToMyaccount(urlString: String?, method: String, parameters:[String: Any]?, completion:@escaping (String?, Bool) ->Void){
        apiCient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error) in
            completion(dict?["address"] as? String, isSuccess!)
        })
    }
    
    func getMultipleAddressFromMyaccount(urlString: String?, method: String, parameters:[String: Any]?, completion:@escaping (String?, Bool) ->Void){
        apiCient.postRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (arrayOfDictionaries) in
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                guard let array = arrayOfDictionaries else {
                    completion(nil,false)
                    return
                }
                self?.addressesArray.removeAll()
                for item in (array) {
                    print(item)
                    let value = ShippingAddress.init(json:item as [String : AnyObject])
                    self?.addressesArray.append(value)
                }
                completion(nil,true)
            }
        })
    }
    func addressesListCount() -> Int? {
        return addressesArray.count ?? 0
    }
    func address_Id(indexPathCell: IndexPath) -> String? {
        return addressesArray[indexPathCell.row].addressId
    }
    func isDefaultAddress(indexPathCell: IndexPath) -> Bool? {
        if addressesArray[indexPathCell.row].defaultAddressId == addressesArray[indexPathCell.row].addressId {
            return true
        }
        return false
    }
    func address_firstAndLastName(indexPathCell: IndexPath) -> String? {
        return addressesArray[indexPathCell.row].firstName! + " " + addressesArray[indexPathCell.row].lastName!
    }
    func address_address(indexPathCell: IndexPath) -> String? {
        return addressesArray[indexPathCell.row].address_1
    }
    func address_city(indexPathCell: IndexPath) -> String? {
        return addressesArray[indexPathCell.row].city
    }
    func address_zipcode(indexPathCell: IndexPath) -> String? {
        return addressesArray[indexPathCell.row].zone_code
    }
}
