//
//  DashBoardViewModel.swift
//  
//
//  Created by Admin on 03/22/18.
//  Copyright © 2017 Erica Millado. All rights reserved.
//

import UIKit
import SwiftyJSON
class AllBrandsNamesViewModel {
    var apiClient = APIClient()
    var allBrandNamesArray:[AllBrandNamesModel] = [AllBrandNamesModel]()
    
    // - This function is what directly accesses the apiClient to make the API call
    func fetchAllBrandNamesList(urlString: String, completion: @escaping () -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.getRequestFetch(urlString: urlString, completion: {[weak self] (arrayOfAppsDictionaries ) in
            
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                print(arrayOfAppsDictionaries!)
                for item in (arrayOfAppsDictionaries)! {
                    print(item)
                    let value = AllBrandNamesModel.init(json:item as [String : AnyObject])
                    self?.allBrandNamesArray.append(value)
                }
                print("allBrandNamesArray :-- \(String(describing: self?.allBrandNamesArray))")
                completion()
            }
        })
    }
    // MARK: - banners data array
    func numberOfSections() -> Int{
        return allBrandNamesArray.count
    }
    // MARK: - values to display in our table view cell
    func brandNameToDisplay(cellIndexPath: IndexPath) -> String {
        return allBrandNamesArray[cellIndexPath.section].name ?? ""
    }
}

