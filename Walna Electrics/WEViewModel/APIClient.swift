
//
//  APIClient.swift
//  MVVMBlog
//
//  Created by Erica Millado on 6/16/17.
//  Copyright © 2017 Erica Millado. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

//1 - This APIClient will be called by the viewModel to get our top 100 app data.
class APIClient {
    
    //2 - the completion handler will be executed after our top 100 app data is fetched
    // our completion handler will include an optional array of NSDictionaries parsed from our retrieved JSON object
    func getRequestFetch(urlString: String, completion: @escaping ([Dictionary<String, Any>]?) -> Void) {
        
        //3 - unwrap our API endpoint
        guard let url = URL(string: urlString) else {
            print("Error unwrapping URL");
            return
        }
        
        //4 - create a session and dataTask on that session to get data/response/error
        Alamofire.request(url).responseData { response in
            //5 - unwrap our returned data
            guard let unwrappedData = response.data else { print("Error getting data"); return }
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: unwrappedData, options: .allowFragments) as? NSDictionary {
                    
                    //7 - create an array of dictionaries from
                    if let apps = responseJSON.value(forKeyPath: "brandsAndSubBrands") as? [Dictionary<String, Any>] {
                        //8 - set the completion handler with our apps array of dictionaries
                        completion(apps)
                    }else if let apps = responseJSON.value(forKeyPath: "banners") as? [Dictionary<String, Any>] {
                        //8 - set the completion handler with our apps array of dictionaries
                        completion(apps)
                    }else if let apps = responseJSON.value(forKeyPath: "brands") as? [Dictionary<String, Any>] {
                        //8 - set the completion handler with our apps array of dictionaries
                        completion(apps)
                    }else if let apps = responseJSON.value(forKeyPath: "products") as? [Dictionary<String, Any>] {
                        //8 - set the completion handler with our apps array of dictionaries
                        completion(apps)
                    }
                    else {
                        print("response success")
                    }
                }
            } catch {
                //9 - if we have an error, set our completion with nil
                completion(nil)
                print("Error getting API data: \(error.localizedDescription)")
            }
        }
    }

    func postRequestFetch(urlString: String?, method: String, parameters:[String: Any]?,  completion: @escaping ([Dictionary<String, Any>]?) -> Void) {
    
    //3 - unwrap our API endpoint
        let CheckValue = Double((parameters?.count)!)

        guard let url = URL(string: urlString!), CheckValue > 0 else {
    print("Error unwrapping URL");
    return
    }
        Alamofire.request(urlString!, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseString {
    response in
    switch response.result {
    case .success:
    print(response.data?.description)
                guard let unwrappedData = response.data else { print("Error getting data"); return }
    do {
        if let responseJSON = try JSONSerialization.jsonObject(with: unwrappedData, options: .allowFragments) as? NSDictionary {
            
            //7 - create an array of dictionaries from
            if let apps = responseJSON.value(forKeyPath: "subBrands") as? [Dictionary<String, Any>] {
                //8 - set the completion handler with our apps array of dictionaries
                completion(apps)
            }else if let apps = responseJSON.value(forKeyPath: "products") as? [Dictionary<String, Any>]{
                completion(apps)
            }else if let apps = responseJSON.value(forKeyPath: "categories") as? [Dictionary<String, Any>]{
                completion(apps)
            }else if let apps = responseJSON.value(forKeyPath: "subSubBrandName") as? [Dictionary<String, Any>]{
                completion(apps)
            }else if let apps = responseJSON.value(forKeyPath: "addresses") as? [Dictionary<String, Any>]{
                completion(apps)
            }
            else {
                print("response success")
            }
        }
    } catch {
        //9 - if we have an error, set our completion with nil
        completion(nil)
        print("Error getting API data: \(error.localizedDescription)")
    }
    break
    case .failure(let error):
    print(error)
    }
    }
    }
    func cart_PostRequestFetch(urlString: String?, method: String, parameters:[String: Any]?,  completion: @escaping (Dictionary<String, Any>?, _ isSuccess: Bool?, _ error: String?) -> Void) {
        
        //3 - unwrap our API endpoint
        let CheckValue = Double((parameters?.count)!)
        
        guard let url = URL(string: urlString!), CheckValue > 0 else {
            print("Error unwrapping URL");
            return
        }
        Alamofire.request(urlString!, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseString {
            response in
            switch response.result {
            case .success:
                print(response.data?.description)
                guard let unwrappedData = response.data else { print("Error getting data"); return }
                do {
                    if let responseJSON = try JSONSerialization.jsonObject(with: unwrappedData, options: .allowFragments) as? NSDictionary {
                        
                        //7 - create an array of dictionaries from
                        if let apps = responseJSON as? Dictionary<String, Any> {
                            //8 - set the completion handler with our apps array of dictionaries
                            //below post address
                            if let postAddrress = apps["address"] as? String {
                                completion(apps, response.result.isSuccess, nil)
                            }else if let postAddrress = apps["address"] as? [Dictionary<String, Any>] {
                               let dict = postAddrress[0]
                                print(dict["success"] as? String)
                            completion(postAddrress[0], response.result.isSuccess, nil)
                            }else {
                            completion(apps, response.result.isSuccess, nil)
                            }

                        }
                        else {
                            
                            print("response success")
                        }
                    }
                } catch {
                    //9 - if we have an error, set our completion with nil
                    completion(nil, false, error.localizedDescription)
                    print("Error getting API data: \(error.localizedDescription)")
                }
                break
            case .failure(let error):
                print(error)
                completion(nil, false, error.localizedDescription)
            }
        }
    }
    func getRequestFetch_BannersAutoDisplay(urlString: String, completion: @escaping ([Dictionary<String, Any>]?, _ bool: Bool,  _ error: String?) -> Void) {
        
        //3 - unwrap our API endpoint
        guard let url = URL(string: urlString) else {
            print("Error unwrapping URL");
            return
        }
        
        //4 - create a session and dataTask on that session to get data/response/error
        Alamofire.request(url).responseData { response in
            //5 - unwrap our returned data
            guard let unwrappedData = response.data else { print("Error getting data"); return }
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: unwrappedData, options: .allowFragments) as? NSDictionary {
                    
                    //7 - create an array of dictionaries from
                    if let apps = responseJSON.value(forKeyPath: "banners") as? [Dictionary<String, Any>] {
                        //8 - set the completion handler with our apps array of dictionaries
                        completion(apps, response.result.isSuccess, nil)
                        
                    }else if let apps = responseJSON.value(forKeyPath: "downloadableBrands") as? [Dictionary<String, Any>] {
                        //8 - set the completion handler with our apps array of dictionaries
                        completion(apps, response.result.isSuccess, nil)
                    }
                    else {
                        print("else other response success")
                    }
                }
            } catch {
                //9 - if we have an error, set our completion with nil
                completion(nil, response.result.isSuccess, error.localizedDescription)
                print("Error getting API data: \(error.localizedDescription)")
            }
        }
    }

}
