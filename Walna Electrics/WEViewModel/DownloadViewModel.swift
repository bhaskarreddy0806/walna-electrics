//
//  DownloadViewModel.swift
//  Walna Electrics
//
//  Created by Admin on 04/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
class DownloadPriceViewModel {
    var apiClient = APIClient()
    var downloadbrandsListArray:[DownloadBrandsListModel] = [DownloadBrandsListModel]()
    var downloadProductsPriceByBrandOrCatalougeListArray:[ProductsPriceByBrandsOrCatalougesListModel] = [ProductsPriceByBrandsOrCatalougesListModel]()
    
    // Fetch download brand list
    func fetchDownloadBrandsAndCatalougesList(urlString: String?, completion: @escaping (Dictionary<String, Any>?, _ isSuccess: Bool?,
        _ error: String?) -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.getRequestFetch_BannersAutoDisplay(urlString: urlString!, completion: {[weak self] (arrayOfAppsDictionaries, isSuccess, error) in
            
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
                print(arrayOfAppsDictionaries!)
                for item in (arrayOfAppsDictionaries)! {
                    print(item)
                    let value = DownloadBrandsListModel.init(json:item as [String : AnyObject])
                    self?.downloadbrandsListArray.append(value)
                }
                print("downloadbrandsListArray :-- \(self?.downloadbrandsListArray)")
                completion(nil, isSuccess, nil)
            }
        })
    }
        func downloadBrandsListCount() -> Int? {
            return downloadbrandsListArray.count ?? 0
        }
        func getBrandId(indexPathCell: IndexPath) -> String? {
            return downloadbrandsListArray[indexPathCell.section].id
        }
        func getBrand_name(indexPathCell: IndexPath) -> String? {
            return downloadbrandsListArray[indexPathCell.section].name
        }
    // Below for Products price for Brands and Catalouges
    func fetchDownloadProducts_BrandsAndProductsList(urlString: String?, method: String, priceBrandOrCatalouge: Int?, parameters:[String: Any]?, completion: @escaping (Dictionary<String, Any>?, _ isSuccess: Bool?,
        _ error: String?) -> Void) {
        //5 - call on the apiClient to fetch the brands and Subbrands
        apiClient.cart_PostRequestFetch(urlString: urlString, method: method, parameters: parameters, completion: {[weak self] (dict, isSuccess, error ) in
            
            DispatchQueue.main.async {
                //7 - assign our local apps array to our returned json
             
                if let dict = dict {
                    
                    let allDownloads = dict["productPriceList"] as? [Dictionary<String, Any>]

                        for item in (allDownloads)! {
                            print(item)
                            let value = ProductsPriceByBrandsOrCatalougesListModel.init(json:item as [String : AnyObject])
                            self?.downloadProductsPriceByBrandOrCatalougeListArray.append(value)
                        }
print("price on Brands catalouge price list ::\(self?.downloadProductsPriceByBrandOrCatalougeListArray)")
                    
                }
                completion(dict, isSuccess, error)
            }
        })
    }
    func downloadBrandsOrCatalougesListCount() -> Int? {
        return downloadProductsPriceByBrandOrCatalougeListArray.count ?? 0
    }
    func downloadedBrandsOrCatalouges_Id(indexPathCell: IndexPath) -> String? {
        return downloadProductsPriceByBrandOrCatalougeListArray[indexPathCell.section].id
    }
    func downloadedBrandsOrCatalouges_name(indexPathCell: IndexPath) -> String? {
        return downloadProductsPriceByBrandOrCatalougeListArray[indexPathCell.section].name
    }
    func downloadedBrandsOrCatalouges_URL(indexPathCell: IndexPath) -> String? {
        return downloadProductsPriceByBrandOrCatalougeListArray[indexPathCell.section].download_url
    }
}
