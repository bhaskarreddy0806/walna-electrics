//
//  ListTableViewCell.swift
//  xxxxx
//
//  Created by Radhika, Muddana (623-Extern) on 21/03/18.
//  Copyright © 2018 Radhika, Muddana (623-Extern). All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    @IBOutlet weak internal var thumbImage: UIImageView!
    @IBOutlet weak internal var titleLabel: UILabel!
    @IBOutlet weak internal var stockLabel: UILabel!
    @IBOutlet weak internal var hsnLabel: UILabel!
    @IBOutlet weak internal var skuNoLabel: UILabel!
    @IBOutlet weak internal var brandLabel: UILabel!
    @IBOutlet weak internal var priceLabel: UILabel!
    @IBOutlet weak internal var quantityCountLabel: UILabel!
    @IBOutlet weak internal var removeItemBtn: UIButton!
    @IBOutlet weak internal var notifyMeBtn: UIButton!

    @IBOutlet weak var stockLabelHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
