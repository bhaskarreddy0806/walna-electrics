//
//  CartModel.swift
//  Walna Electrics
//
//  Created by Admin on 26/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
class CartListModel {
    let cart_id:String?
    let product_id:String?
    let thumb:String?
    let name:String?
    let description:String?
    let model:String?
    let catalogue_no:String?
    let hsn:String?
    let discount:String?
    let special:Int?
    let recurring:String?
    let quantity:String?
    let stock:String?
    let reward:String?
    let price:Int?
    let price_without_round:Float?
    let rating:Int?
    let total:Int?
    let reviews:Int?

    init(json : [String: AnyObject]) {
        self.cart_id   = json["cart_id"] as? String
        self.product_id    = json["product_id"] as? String
        self.thumb          = json["thumb"] as? String
        self.name           = json["name"] as? String
        self.description           = json["description"] as? String
        self.model          = json["model"] as? String
        self.catalogue_no          = json["catalogue_no"] as? String
        self.hsn            = json["hsn"] as? String
        self.discount       = json["discount"] as? String
        self.special        = json["special"] as? Int
        self.recurring       = json["recurring"] as? String
        self.quantity       = json["quantity"] as? String
        self.stock       = json["stock"] as? String
        self.reward       = json["reward"] as? String
        self.price          = json["price"] as? Int
        self.price_without_round = json["price_without_round"] as? Float
        self.rating         = json["rating"] as? Int
        self.total         = json["total"] as? Int
        self.reviews         = json["reviews"] as? Int
    }
}
