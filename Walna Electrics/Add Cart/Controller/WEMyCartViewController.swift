//
//  WEMyCartViewController.swift
//  Walna Electrics
//
//  Created by Admin on 22/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
class WEMyCartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, WEAlertViewDelegate {
        @IBOutlet weak var listRequestTableView: UITableView!
    var cartViewModel = CartViewModel()
    var categoryProductNameName = ""
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var clearAllButton: UIButton!
    @IBOutlet weak var proceedToPayButton: UIButton!
    @IBOutlet weak var totalAmountButton: UIButton!
    var activityView = NDActivityViewHelper()
    var weAlertView = WEAlertView.alertViewCustom()
    
        override func viewDidLoad() {
            super.viewDidLoad()
            getCartProductsRequest()
           // listRequestTableView.rowHeight = UITableViewAutomaticDimension
            //listRequestTableView.estimatedRowHeight = 44
        }
    func getCartProductsRequest() {
        // remove all items from cart
        let api:String = Constants().qaUrl2 + Constants().getCartProductsEndPoint
        let params = ["customer_id":"3"]
        cartViewModel.getCartProducts(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (dict, isSuccess, error ) in
            if(isSuccess)!{
                self?.listRequestTableView.delegate = self
                self?.listRequestTableView.dataSource = self
                self?.listRequestTableView.reloadData()
                print(self?.cartViewModel.getTotalCartPrice())
                self?.totalAmountButton.setTitle(self?.cartViewModel.getTotalCartPrice(), for: .normal)
                //let count = dict!["products"] as? [Dictionary<String, Any>]
                //self.cartCount.setTitle((count?.count.description)!, for: .normal)
            }
        }
    }
    // MARK: - UIButton actions
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func homeButtonAction(_ sender: Any) {
        showLogin()
    }
    @IBAction func clearAllButtonAction(_ sender: Any) {
        
        weAlertView.frame = self.view.bounds
        weAlertView.setMessage(message: "Sure! \n You want to clear all the items from Cart List")
        weAlertView.delegate = self
        self.view.addSubview(weAlertView)
    }
    @IBAction func proceedToPayButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"WEAddressViewController") as? WEAddressViewController
        self.present(viewController!, animated: true)
    }
    // MARK: - AlertView delegates
    func okButtonTapped() {
        activityView.startActivity(target: self)
        let params = ["customer_id":"3"]
        print("params :- \(params)")
        let api:String = Constants().qaUrl2 + Constants().clearCartEndPoint
        cartViewModel.removeItemFromCartList(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (dict, isSuccess, error ) in
            if(isSuccess)!{
                self?.activityView.stopActivity()
                self?.cartViewModel.removeAllItemAtCartList()
                self?.dismiss(animated: false, completion: nil)
                MessageManager.showAlert(title: "Walna Electrics", message: "Removed all items from your Cart successfully", imageName: nil)
                cartCountDynamic = "0"
                print("removed item from Cart successfully")
            }
        }
    }
    func cancelButtonTapped() {
        
    }
    
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    }
    extension WEMyCartViewController {
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 10
        }
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView()
            headerView.frame = CGRect(x:0, y:0, width:StringConstant.MAIN_SCREEN_WD, height:5)
            headerView.backgroundColor = UIColor.clear
            return headerView
        }

        func numberOfSections(in tableView: UITableView) -> Int {
            return cartViewModel.allCartItemsCount()!
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as? ListTableViewCell else {
                fatalError("Cell Doenot exist")
                return UITableViewCell()
            }
            cell.titleLabel.text = cartViewModel.cartDescription(indexPathCell: indexPath)
            cell.hsnLabel.text = cartViewModel.cart_hsn(indexPathCell: indexPath)
            cell.skuNoLabel.text = cartViewModel.cart_skuNo(indexPathCell: indexPath)
            cell.brandLabel.text = cartViewModel.cart_model(indexPathCell: indexPath)

            cell.thumbImage.imageFromServerURL(urlString: cartViewModel.cart_thumbImageUrl(indexPathCell: indexPath)!)
            cell.priceLabel.text = "₹ " + String(cartViewModel.cart_price(indexPathCell: indexPath)!)
            cell.quantityCountLabel.text = cartViewModel.cart_quantityNo(indexPathCell: indexPath)
            if cartViewModel.cart_isStock(indexPathCell: indexPath) ==  "In Stock"{
                cell.stockLabelHeightConstraint.constant = 20
                cell.stockLabel.text = "In Stock"
                cell.stockLabel.textColor = .green
            } else {
                cell.stockLabelHeightConstraint.constant = 20
                cell.stockLabel.text = "Out of Order"
                cell.stockLabel.textColor = .red
            }
            cell.removeItemBtn?.addTarget(self
                , action: #selector(removeItemCartAction(_:)), for: .touchUpInside)
            return cell
         }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        }
        @objc func removeItemCartAction(_ sender: AnyObject) {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.listRequestTableView)
           let indexPath = self.listRequestTableView.indexPathForRow(at:buttonPosition)
            print("indexPath selected :- \(indexPath?.section)")
            let params = ["cart_id":"\(cartViewModel.getCartId(indexPathCell:(indexPath)!)!)","customer_id":"3"]
            print("params :- \(params)")
            let api:String = Constants().qaUrl2 + Constants().removeCartProductsEndPoint
            cartViewModel.removeItemFromCartList(urlString: api, method: StringConstant.METHOD_POST, parameters: params){[weak self] (dict, isSuccess, error ) in
                if(isSuccess)!{
                    print("removed item from cart successfully")
                    self?.cartViewModel.removeItemAtCartList(indexPathCell: indexPath!)
                    self?.listRequestTableView.reloadData()                }
            }
        }
}

