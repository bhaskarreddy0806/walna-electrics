//
//  SecondCell.swift
//  MVVM_Demo
//
//  Created by Shiv on 04/04/18.
//  Copyright © 2018 TD. All rights reserved.
//

import UIKit

class ShippingAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var detailContainerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        detailContainerView.addDashedBorder(strokeColor: UIColor.black.withAlphaComponent(0.5), lineWidth: 1, size: detailContainerView.frame.size)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension UIView {
    
    func addDashedBorder(strokeColor: UIColor, lineWidth: CGFloat, size: CGSize) {
        self.layoutIfNeeded()
        let strokeColor = strokeColor.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let containerSize = CGSize(width: UIScreen.main.bounds.width-16, height: size.height)
        let frameSize = containerSize
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = strokeColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [5,5] // adjust to your liking
        shapeLayer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: shapeRect.width, height: shapeRect.height), cornerRadius: self.layer.cornerRadius).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
}
