//
//  NDMenuTableViewCell.swift
//  NestleDeserts
//
//  Created by Radhika on 03/02/17.
//  Copyright © 2017 Radhika. All rights reserved.
//

import UIKit

class NDMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellTextLabel: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        notificationCornerRadius()
        cellImageView.backgroundColor = .red
    }
    func notificationCornerRadius()
    {
        notificationLabel.layer.cornerRadius = notificationLabel.frame.size.height/2
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
