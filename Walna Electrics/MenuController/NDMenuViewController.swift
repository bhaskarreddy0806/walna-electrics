//
//  NDMenuViewController.swift
//  NestleDeserts
//
//  Created by Radhika on 03/02/17.
//  Copyright © 2017 Radhika. All rights reserved.
//

import UIKit
protocol MenuViewControllerDelegateNew {
    func moveToController(cellNo:NSInteger)
    func menuViewTapped()
    func profileEditClicked()
}
class NDMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    //MARK:- IBOutlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var youtubeButton: UIButton!
    @IBOutlet weak var instagramButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    //MARK:- Variable intialize
    var menuArray = NSArray()
    var menuImageArray = NSArray()
    var delegate:MenuViewControllerDelegateNew?
    //MARK:- View methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        initialize()
    }
    @objc func oriantationChange()
    {
        profile()
    }
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        profile()
        self.view.layoutIfNeeded()
    }
    //MARK:- Initialize method
    func initialize()
    {
        let str:NSString = "http://www.gravatar.com/avatar/?d=identicon"
        self.profileImageView.imageFromServerURL(urlString:str as String)
        if isGuestUser == false {
            profileNameLabel.text = "bhaskar reddy"
        }else{
            profileNameLabel.text = "Guest User"
        }
        tableView.register(UINib(nibName: "NDMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCellId")
    }
    
    func profile()
    {
        profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2
        self.view.layoutIfNeeded()
        if isGuestUser == false {
            profileNameLabel.text = "Bhaskar reddy"
        }else{
            profileNameLabel.text = "Guest User"
        }
        profileImageView.layer.masksToBounds = true
    }
    func unHideNotificationLabel(cell:NDMenuTableViewCell,forIndex:NSInteger)
    {
        if (forIndex == 4)
        {
            // cell.notificationLabel.isHidden = false
        }
    }
    //MARK:- Table View Delegate methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCellId", for: indexPath) as! NDMenuTableViewCell
        cell.cellTextLabel.text = menuArray.object(at: indexPath.row) as? String
            cell.isUserInteractionEnabled = true
            cell.cellTextLabel.textColor = UIColor.white
           // cell.cellImageView.image = UIImage(named: (menuImageArray.object(at: indexPath.row) as? String)!)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return menuArray.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        delegate?.moveToController(cellNo: indexPath.row)
    }
    //MARK:- Button Actions
   
    //MARK:- Ui Touch delegate methdos
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let touche = touches.first
        if (touche?.view == mainView)
        {
            delegate?.menuViewTapped()
        }
    }
}
