//
//  LeftMenuViewController.swift
//  NestleDeserts
//
//  Created by Radhika on 03/02/17.
//  Copyright © 2017 Radhika. All rights reserved.
//

import UIKit
protocol MenuViewControllerDelegate
{
    func didSelectedCell(cellNo:NSInteger)
    func menuViewTapped()
    func socialButtonClicked()
}
class LeftMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var menuArray = NSArray()
    var menuImageArray = NSArray()
    var controllerIdentifirArray = NSArray()
    var delegate:MenuViewControllerDelegate?
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        initialize()
    }
    func initialize()
    {
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCellId")
        menuArray = ["Dessert Recipes","Our Products","My Favourites","Recipe Generator","Substitute Listing","Cooking Timer","Unit Converter"]
        NotificationCenter.default.addObserver(self, selector: #selector(oriantationChange), name: Notification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    @objc func oriantationChange()
    {
        profile()
    }
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        profile()
    }
    func profile()
    {
        profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.borderWidth = 2.0
        profileImageView.layer.masksToBounds = true
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCellId", for: indexPath) as! NDMenuTableViewCell
        cell.cellTextLabel.text = menuArray.object(at: indexPath.row) as? String
        cell.cellImageView?.image = UIImage(named: (menuImageArray.object(at: indexPath.row) as? String)!)

        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menuArray.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("didSelectRowAt \(indexPath.row)")
        delegate?.didSelectedCell(cellNo: indexPath.row)
       // Utility.navigateTo(target: self, controllerIdentifier: (controllerIdentifirArray.object(at: indexPath.row) as? String)!)
    }
    //MARK:- Ui Touch delegate methdos
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let touche = touches.first
        if (touche?.view == mainView)
        {
            delegate?.menuViewTapped()
        }
    }
}
