//
//  NDMenuView.swift
//  NestleDeserts
//
//  Created by Shiv on 13/02/17.
//  Copyright © 2017 Radhika. All rights reserved.
//

import UIKit
protocol NDMenuViewDelegate {
    func menuClosed()
    func moveNextController(cellIndex:NSInteger)
}
class NDMenuView: NSObject,MenuViewControllerDelegateNew
{
    func socialButtonClicked() {
        
    }
    
    var delegate:NDMenuViewDelegate?
    
    var checkMenuStatus:Bool = false
    var checkArabicMenuStatus:Bool = false
    var engLanguage = Bool()
    var menuController = NDMenuViewController()
    var menuArray = NSArray()
    var menuImageArray = NSArray()
    var menuGuestImageArray = NSArray()
    var referenceView = UIView()
    var referenceViewController = UIViewController()
    func openMenu(target:UIViewController){
        NotificationCenter.default.addObserver(self, selector: #selector(cancelButtonTapped), name: NSNotification.Name(rawValue: "logoutClicked"), object: nil)
        target.view.endEditing(true)
        menuArray = ["Home","Shop By Brands","Shop By Products","Bulk Order","Multi Compare","Download Price List","Download Catalogue List"]
        menuImageArray = ["ras.png","ras.png","ras.png","ras.png","ras.png"]
        englishMenu(target:target)
    }
    func englishMenu(target:UIViewController){
        menuController.delegate = self as? MenuViewControllerDelegateNew
        referenceView = target.view
        referenceViewController = target
        if checkMenuStatus
        {
            closeEngMenu(sender: "closeEngMenu:")
        }else{
            menuController.view.frame = CGRect(x: -target.view.frame.size.width, y: 0, width: target.view.frame.size.width, height: target.view.frame.size.height)
            menuController.view.alpha = 0.80
            menuController.menuArray = menuArray
            menuController.menuImageArray = menuImageArray

            UIView.animate(withDuration: 0.3, animations:
                {
                    self.menuController.view.frame.origin.x  = 0
                    self.menuController.view.alpha = 1.0
            })
            target.view.addSubview(menuController.view)
        }
        let swippedLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.closeEngMenu))
        swippedLeft.direction = UISwipeGestureRecognizerDirection.left
        target.view.addGestureRecognizer(swippedLeft)

     
        
    }

    @objc func closeEngMenu(sender: Any?){
        UIView.animate(withDuration: 0.3, animations: {
            self.menuController.view.frame.origin.x  = -self.referenceView.frame.size.width
        }, completion: {
            (status:Bool) in
            self.menuController.view.removeFromSuperview()
            self.delegate?.menuClosed()
        })
    }
    func closeMenuWithoutAnimation(){
        closeEngMenuWihoutAnimation()
    }
    func closeEngMenuWihoutAnimation(){
        self.menuController.view.frame.origin.x  = -self.referenceView.frame.size.width
        self.menuController.view.removeFromSuperview()
    }
    func didSelectedCell(cellNo: NSInteger){
        closeEngMenu(sender: nil)
    }
    
    func menuViewTapped(){
        closeEngMenu(sender: nil)
    }
    func moveToController(cellNo: NSInteger) {
        switch (cellNo) {
        case 0:
            closeEngMenu(sender: nil)
            break
        case 1:
            closeEngMenu(sender: nil)
            delegate?.moveNextController(cellIndex:cellNo)
            closeEngMenu(sender: nil)

            break
        case 2:
            closeEngMenu(sender: nil)

            delegate?.moveNextController(cellIndex:cellNo)

            break
        case 3:
            closeEngMenu(sender: nil)

            delegate?.moveNextController(cellIndex:cellNo)

            break
        case 4:
            closeEngMenu(sender: nil)

            break
        case 5:
            closeEngMenu(sender: nil)

            delegate?.moveNextController(cellIndex:cellNo)
            break
        case 6:
            closeEngMenu(sender: nil)

            delegate?.moveNextController(cellIndex:cellNo)
            break
        default:
            print("default")
        }
    }
    // MARK:- logout Delegate method
    @objc func cancelButtonTapped() {
    }
    internal func profileEditClicked() {

    }
    //MARK: -  web url call
}
